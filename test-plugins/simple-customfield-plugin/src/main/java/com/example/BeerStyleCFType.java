package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.StringConverterImpl;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.impl.RenderableTextCFType;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

import com.google.common.base.Joiner;

public class BeerStyleCFType extends RenderableTextCFType
{
    public static final String CF_KEY = "com.example.simple-customfield-plugin:beer-style-cf";


    public BeerStyleCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager)
    {
        super(customFieldValuePersister, new StringConverterImpl(), genericConfigManager);
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        //add our beer styles to the context
        List<String> styleNames = new ArrayList<String>();
        styleNames.add("Pale Ale");
        styleNames.add("Porter");
        styleNames.add("Stout");

        map.put("beerStyles", Joiner.on(",").join(styleNames));

        return map;
    }

}