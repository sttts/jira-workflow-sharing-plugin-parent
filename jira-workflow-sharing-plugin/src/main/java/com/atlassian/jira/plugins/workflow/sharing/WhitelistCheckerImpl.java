package com.atlassian.jira.plugins.workflow.sharing;

import org.springframework.beans.factory.InitializingBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class WhitelistCheckerImpl implements WhitelistChecker, InitializingBean
{
    private Set<String> allowedClassNames;

    @Override
    public void afterPropertiesSet() throws IOException
    {
        readFromResource("/whitelist.txt");
    }

    public void readFromResource(String resourceName) throws IOException
    {
        allowedClassNames = new HashSet<String>();

        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(resourceName)));

            String line;
            while ((line = reader.readLine()) != null)
            {
                allowedClassNames.add(line);
            }
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
        }
    }


    @Override
    public boolean isAllowed(String className)
    {
        return allowedClassNames.contains(className);
    }
}
