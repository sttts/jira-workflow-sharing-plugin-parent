package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.importer.ImporterConstants;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.sal.api.message.I18nResolver;

import java.io.IOException;
import java.io.InputStream;

public class DefaultWorkflowBundleFactory implements WorkflowBundle.Factory
{
    private final BundleParser bundleParser;

    public DefaultWorkflowBundleFactory(final I18nResolver helper)
    {
        this(helper, ImporterConstants.MAX_STREAM, ImporterConstants.MAX_ENTRY_BYTES);
    }

    public DefaultWorkflowBundleFactory(final I18nResolver resolver, long maxStream, long maxEntry)
    {
        bundleParser = new BundleParser(new I18nResolverErrorMessageProvider(resolver), maxStream, maxEntry);
    }

    @Override
    public WorkflowBundle bundle(InputStream stream, WorkflowBundle.BundleSource source) throws IOException, ValidationException
    {
        BundleParser.Builder builder = bundleParser.parse(stream, true);

        return builder.setSource(source).build();
    }

    private class I18nResolverErrorMessageProvider implements BundleParser.ErrorMessageProvider
    {
        private final I18nResolver i18n;

        I18nResolverErrorMessageProvider(I18nResolver i18n)
        {
            this.i18n = i18n;
        }

        @Override
        public String notZip()
        {
            return i18n.getText("wfshare.exception.file.must.be.a.zip.file");
        }

        @Override
        public String bundleEntryTooBig(long size, long maxSize)
        {
            return i18n.getText("wfshare.exception.workflow.too.big");
        }

        @Override
        public String invalidJson(String fileName)
        {
            return i18n.getText("wfshare.exception.invalid.file", fileName);
        }

        @Override
        public String unknownFile(String fileName)
        {
            return i18n.getText("wfshare.exception.unknown.file", fileName);
        }

        @Override
        public String noWorkflowXml()
        {
            return i18n.getText("wfshare.exception.workflow.xml.not.found.in.zip");
        }
    }
}
