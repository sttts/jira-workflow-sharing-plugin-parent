package com.atlassian.jira.plugins.workflow.sharing.exporter.component;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.plugins.workflow.sharing.*;
import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManager;
import com.atlassian.jira.plugins.workflow.sharing.util.IOSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.sal.api.message.I18nResolver;
import com.sysbliss.jira.plugins.workflow.WorkflowDesignerConstants;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class JiraWorkflowSharingExporterImpl implements JiraWorkflowSharingExporter
{
    public static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";

    private final WorkflowService workflowService;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final WorkflowCustomFieldsHelper workflowCustomFieldsHelper;
    private final I18nResolver i18n;
    private final WorkflowLayoutKeyFinder workflowLayoutKeyFinder;
    private final FileManager fileManager;

    public JiraWorkflowSharingExporterImpl(WorkflowDesignerPropertySet workflowDesignerPropertySet, WorkflowService workflowService,
                                           WorkflowExtensionsHelper workflowExtensionsHelper, JiraAuthenticationContext jiraAuthenticationContext,
                                           WorkflowScreensHelper workflowScreensHelper, WorkflowCustomFieldsHelper workflowCustomFieldsHelper, I18nResolver i18n,
                                           WorkflowLayoutKeyFinder workflowLayoutKeyFinder, FileManager fileManager)
    {
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.workflowService = workflowService;
        this.workflowExtensionsHelper = workflowExtensionsHelper;

        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowScreensHelper = workflowScreensHelper;
        this.workflowCustomFieldsHelper = workflowCustomFieldsHelper;
        this.i18n = i18n;
        this.workflowLayoutKeyFinder = workflowLayoutKeyFinder;
        this.fileManager = fileManager;
    }

    @Override
    public ExportResult exportActiveWorkflow(String name, String notes) throws CanNotCreateFileException
    {
        ExportResult result = null;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());
        JiraWorkflow workflow = workflowService.getWorkflow(jiraServiceContext, name);
        
        if (null != workflow)
        {
            try
            {
                String layoutKey = workflowLayoutKeyFinder.getActiveLayoutKey(workflow.getName());
                String annotationKey = WorkflowDesignerConstants.ANNOTATION_PREFIX.concat(workflow.getName());
                result = exportWorkflow(workflow, layoutKey, annotationKey, notes);
            }
            catch (CanNotCreateFileException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }

        return result;
    }

    @Override
    public ExportResult exportDraftWorflow(String name, String notes) throws CanNotCreateFileException
    {
        ExportResult result = null;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());
        JiraWorkflow workflow = workflowService.getDraftWorkflow(jiraServiceContext, name);

        if (null != workflow)
        {
            try
            {
                String layoutKey = workflowLayoutKeyFinder.getDraftLayoutKey(workflow.getName());
                String annotationKey = WorkflowDesignerConstants.ANNOTATION_DRAFT_PREFIX.concat(workflow.getName());
                result = exportWorkflow(workflow, layoutKey, annotationKey, notes);
            }
            catch (CanNotCreateFileException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
        return result;
    }

    private ExportResult exportWorkflow(JiraWorkflow workflow, String layoutKey, String annotationKey, String notes) throws Exception
    {
        workflow = workflowExtensionsHelper.copyAndRemoveIllegalComponents(workflow);

        String workflowXml = WorkflowUtil.convertDescriptorToXML(workflow.getDescriptor());
        String layoutJson = workflowDesignerPropertySet.getProperty(layoutKey);
        String annotationJson = workflowDesignerPropertySet.getProperty(annotationKey);

        String customFieldsJson = workflowCustomFieldsHelper.getCustomFieldsJson(workflowScreensHelper.getCustomFieldIdsForWorkflowScreens(workflow));
        String screensJson = workflowScreensHelper.getScreensJson(workflow);

        ZipOutputStream zipOutputStream = null;
        File zipFile = null;
        try
        {
            zipFile = fileManager.createExportedWorkflowFile();
            zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));

            if (StringUtils.isNotBlank(workflowXml))
            {
                addStringToZip(WorkflowSharingFiles.WORKFLOW.getPath(), workflowXml, zipOutputStream);
            }
            else
            {
                throw new IOException(i18n.getText("wfshare.exception.workflow.xml.is.blank"));
            }

            if (StringUtils.isNotBlank(layoutJson))
            {
                addStringToZip(WorkflowSharingFiles.LAYOUT.getPath(), layoutJson, zipOutputStream);
            }

            if (StringUtils.isNotBlank(annotationJson))
            {
                addStringToZip(WorkflowSharingFiles.ANNOTATION.getPath(), annotationJson, zipOutputStream);
            }

            if(StringUtils.isNotBlank(customFieldsJson)&& !customFieldsJson.equals("[]"))
            {
                addStringToZip(WorkflowSharingFiles.CUSTOM_FIELDS.getPath(), customFieldsJson, zipOutputStream);
            }

            if(StringUtils.isNotBlank(screensJson)&& !screensJson.equals("[]"))
            {
                addStringToZip(WorkflowSharingFiles.SCREENS.getPath(), screensJson, zipOutputStream);
            }
            
            if (StringUtils.isNotBlank(notes))
            {
                addStringToZip(WorkflowSharingFiles.NOTES.getPath(), notes, zipOutputStream);
            }
        }
        finally
        {
            IOUtils.closeQuietly(zipOutputStream);
        }

        String workflowNameAsFileName = workflow.getName().replaceAll(CLEAN_FILENAME_PATTERN, "-");
        if (workflow.isDraftWorkflow())
        {
            workflowNameAsFileName = workflowNameAsFileName.concat("-draft");
        }
        workflowNameAsFileName = workflowNameAsFileName.concat(".jwb");

        return new ExportResult(zipFile, workflowNameAsFileName);
    }

    protected void addStringToZip(String filename, String content, ZipOutputStream zipOutputStream) throws IOException
    {
        zipOutputStream.putNextEntry(new ZipEntry(filename));
        IOSupport.writeString(content, zipOutputStream);
        zipOutputStream.closeEntry();
    }
}