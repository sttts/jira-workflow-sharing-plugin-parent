package com.atlassian.jira.plugins.workflow.sharing;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

/**
 * This is copied from jira-workflow-designer code since it's not exported for use by other bundles
 */
public abstract class WorkflowLayoutPropertyKeyBuilder
{
    private String workflowName;

    private WorkflowState workflowState;

    protected WorkflowState getWorkflowState()
    {
        return workflowState;
    }

    
    protected String getWorkflowName()
    {
        return workflowName;
    }

    public static WorkflowLayoutPropertyKeyBuilder newBuilder()
    {
        return new WorkflowLayoutPropertyKeyBuilder.MD5();
    }

    public WorkflowLayoutPropertyKeyBuilder setWorkflowName(final String workflowName)
    {
        this.workflowName = workflowName;
        return this;
    }

    public WorkflowLayoutPropertyKeyBuilder setWorkflowState(final WorkflowState workflowState)
    {
        this.workflowState = workflowState;
        return this;
    }

    public abstract String build();

    private static class MD5 extends WorkflowLayoutPropertyKeyBuilder
    {
        @Override
        public String build()
        {
            return getWorkflowState().keyPrefix() + md5Hex(getWorkflowName());
        }
    }

    public enum WorkflowState
    {
        LIVE
                {
                    @Override
                    String keyPrefix()
                    {
                        return "jira.workflow.layout:";
                    }
                },
        DRAFT
                {
                    @Override
                    String keyPrefix()
                    {
                        return "jira.workflow.draft.layout:";
                    }
                };

        abstract String keyPrefix();
    }
}