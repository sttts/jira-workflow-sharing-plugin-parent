package com.atlassian.jira.plugins.workflow.sharing.exporter.servlet;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugins.workflow.sharing.exporter.WorkflowExportNotesProvider;
import com.atlassian.jira.plugins.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;
import com.atlassian.jira.plugins.workflow.sharing.file.FileCleaningJobScheduler;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManager;
import com.atlassian.jira.plugins.workflow.sharing.servlet.AbstractServletWizardHandler;
import com.atlassian.jira.plugins.workflow.sharing.servlet.DataNotFoundInSessionException;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ServletMapping;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.http.JiraHttpUtils;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Multimap;
import com.opensymphony.workflow.FactoryException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public class ExportWizardHandler extends AbstractServletWizardHandler
{
    private final Logger LOG = LoggerFactory.getLogger(ExportWizardHandler.class);

    private enum SessionVar
    {
        WFSHARE_EXPORT_WFNAME, WFSHARE_EXPORT_WFMODE, WFSHARE_EXPORT_NOTES, GENERATED_NOTES_HASH
    }

    public static final String WF_NAME_FIELD_NAME = "workflowName";
    public static final String WF_MODE_FIELD_NAME = "workflowMode";
    public static final String NOTES_FIELD_NAME = "notes";
    public static final String EXPORTED_FILE = "exportedFile";
    public static final String EXPORTED_FILE_NAME = "exportedFileName";
    public static final String MANUAL_NOTES = "manualNotes";
    public static final String GENERATED_NOTES = "generatedNotes";
    
    private final TemplateRenderer renderer;
    private final JiraWorkflowSharingExporter jiraWorkflowSharingExporter;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final VelocityRequestContextFactory requestContextFactory;
    private final WorkflowExportNotesProvider workflowNotesProvider;
    private final FileManager fileManager;
    private final ErrorRenderer errorRenderer;
    private final FileCleaningJobScheduler fileCleaningJobScheduler;
    private final HelpPathResolver helpPathResolver;

    protected ExportWizardHandler(LoginUriProvider loginUriProvider, UserManager userManager,
                                  TemplateRenderer renderer, JiraWorkflowSharingExporter jiraWorkflowSharingExporter,
                                  WorkflowExtensionsHelper workflowExtensionsHelper, VelocityRequestContextFactory requestContextFactory,
                                  WorkflowExportNotesProvider workflowNotesProvider, I18nResolver i18n, FileManager fileManager,
                                  ErrorRenderer errorRenderer, FileCleaningJobScheduler fileCleaningJobScheduler,
                                  XsrfTokenValidator xsrfTokenValidator, XsrfTokenGenerator xsrfTokenGenerator,
                                  ApplicationProperties applicationProperties, HelpPathResolver helpPathResolver)
    {
        super(loginUriProvider, userManager, i18n, xsrfTokenValidator, xsrfTokenGenerator, applicationProperties);
        this.renderer = renderer; 
        this.jiraWorkflowSharingExporter = jiraWorkflowSharingExporter; 
        this.workflowExtensionsHelper = workflowExtensionsHelper; 
        this.requestContextFactory = requestContextFactory;
        this.workflowNotesProvider = workflowNotesProvider;
        this.fileManager = fileManager;
        this.errorRenderer = errorRenderer;
        this.fileCleaningJobScheduler = fileCleaningJobScheduler;
        this.helpPathResolver = helpPathResolver;
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response, RequestHandler handler) throws IOException
    {
        if (enforceAdminLogin(request, response))
        {
            return;
        }

        JiraHttpUtils.setNoCacheHeaders(response);

        response.setContentType("text/html;charset=utf-8");//NON-NLS

        //make sure login redirect info is cleared
        super.clearSessionAttributes(request.getSession());

        Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());//NON-NLS

        ServletMapping requestMapping = ServletMapping.fromPath(getMappingPath(request));

        context.put("cancelUrl", getCancelUrl(request));

        try
        {
            handler.handle(request, response, context, requestMapping);
        }
        catch (DataNotFoundInSessionException e)
        {
            response.sendRedirect(ServletMapping.START_EXPORT.getPath() + "?" + DATA_NOT_FOUND_IN_SESSION_PARAM + "=true");
        }
        catch (CanNotCreateFileException e)
        {
            String error = i18n.getText("wfshare.export.could.not.write.file");
            errorRenderer.render(error, response, context);
        }
        catch (Exception e)
        {
            //We don't know if this is an expected exception or not (i.e. IO error because client disconnected).
            // So we can't just log it.
            LOG.debug("Unable to export workflow", e);
            String message = e.getMessage();
            if (StringUtils.isBlank(message))
            {
                message = i18n.getText("wfshare.export.error.generic");
            }
            errorRenderer.render(message, response, context);
        }
    }

    private String getCancelUrl(HttpServletRequest request)
    {
        String wfName = getSessionAttributeOrNull(request, SessionVar.WFSHARE_EXPORT_WFNAME.name());
        String wfMode = getSessionAttributeOrNull(request, SessionVar.WFSHARE_EXPORT_WFMODE.name());

        if (wfName == null)
        {
            wfName = request.getParameter(WF_NAME_FIELD_NAME);
        }
        if (wfMode == null)
        {
            wfMode = request.getParameter(WF_MODE_FIELD_NAME);
        }

        if (wfName == null || wfMode == null)
        {
            return request.getContextPath() + "/secure/admin/workflows/ListWorkflows.jspa";
        }

        return request.getContextPath() + "/secure/admin/workflows/ViewWorkflowSteps.jspa?workflowMode=" + wfMode
                + "&workflowName=" + JiraUrlCodec.encode(wfName);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        handleRequest(request, response, new RequestHandler()
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ServletMapping requestMapping)
                    throws DataNotFoundInSessionException, IOException, FactoryException, ValidationException
            {
                switch (requestMapping)
                {
                    case START_EXPORT:
                        showStartExport(request, response, context);
                        break;
                    case EXPORT_ADD_NOTES:
                        showAddNotes(request, response, context);
                        break;
                    case EXPORT_WORKFLOW_SUCCESS:
                        showSuccess(request, response, context);
                        break;
                    case NOT_MAPPED:
                        showStartExport(request, response, context);
                        break;
                }
            }
        });
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        handleRequest(request, response, new RequestHandler()
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ServletMapping requestMapping)
                    throws DataNotFoundInSessionException, IOException, FactoryException, ValidationException, CanNotCreateFileException, ServletException
            {
                switch (requestMapping)
                {
                    case START_EXPORT:
                        doStartExport(response);
                        break;
                    case EXPORT_ADD_NOTES:
                        doAddNotes(request, response);
                        break;
                }
            }
        });
    }

    private void showStartExport(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws ValidationException, IOException
    {
        String dataNotFoundInSession = request.getParameter(DATA_NOT_FOUND_IN_SESSION_PARAM);
        if (StringUtils.isNotEmpty(dataNotFoundInSession))
        {
            context.put("errorMessage", i18n.getText("wfshare.exception.export.data.not.found.in.session"));
        }
        else
        {
            String wfName = getSessionAttributeOrNull(request,SessionVar.WFSHARE_EXPORT_WFNAME.name());

            Pair<String,String> nameAndMode = extractWorkflowNameAndMode(request);
            if(null != nameAndMode)
            {
                wfName = nameAndMode.first();
                String wfMode = nameAndMode.second();

                request.getSession().setAttribute(SessionVar.WFSHARE_EXPORT_WFNAME.name(), wfName);
                request.getSession().setAttribute(SessionVar.WFSHARE_EXPORT_WFMODE.name(), wfMode);
            }

            if (StringUtils.isBlank(wfName) && request.getParameter(DATA_NOT_FOUND_IN_SESSION_PARAM) == null)
            {
                throw new ValidationException(i18n.getText("wfshare.exception.no.workflow.name.param"));
            }

            context.put("workflowName", wfName);

            context.put("helpPath", helpPathResolver.getHelpPath("workflow_sharing"));
        }

        setNavigationPaths(context, ServletMapping.START_EXPORT);
        renderer.render(ServletMapping.START_EXPORT.getResultTemplate(), context, response.getWriter());
    }

    private void doStartExport(HttpServletResponse response) throws IOException
    {
        response.sendRedirect(ServletMapping.EXPORT_ADD_NOTES.getPath());
    }

    private void showAddNotes(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException, DataNotFoundInSessionException
    {
        Pair<String,String> nameAndMode = extractWorkflowNameAndModeFromSession(request);
        JiraWorkflow workflow = workflowExtensionsHelper.getWorkflowForNameAndMode(nameAndMode.first(), nameAndMode.second());

        String notes = workflowNotesProvider.createNotes(workflow);
        context.put("notes", notes);

        String notesHash = StringUtils.isEmpty(notes) ? null : md5Hex(notes);
        request.getSession().setAttribute(SessionVar.GENERATED_NOTES_HASH.name(), notesHash);

        addXsrfToken(context, request);

        setNavigationPaths(context, ServletMapping.EXPORT_ADD_NOTES);
        renderer.render(ServletMapping.EXPORT_ADD_NOTES.getResultTemplate(), context, response.getWriter());
    }
    
    private void doAddNotes(HttpServletRequest request, HttpServletResponse response) throws DataNotFoundInSessionException, IOException, ValidationException, CanNotCreateFileException, ServletException
    {
        if (checkXsrf(request, response))
        {
            return;
        }

        String notes = request.getParameter(NOTES_FIELD_NAME);

        String wfName = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFNAME.name());
        String wfMode = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFMODE.name());

        JiraWorkflowSharingExporter.ExportResult result;

        if ("draft".equalsIgnoreCase(wfMode))//NON-NLS
        {
            result = jiraWorkflowSharingExporter.exportDraftWorflow(wfName, notes);
        }
        else
        {
            result = jiraWorkflowSharingExporter.exportActiveWorkflow(wfName, notes);
        }

        if (null == result)
        {
            throw new ValidationException(i18n.getText("wfshare.exception.null.workflow.zip", wfName));
        }

        fileManager.addExportedFileNameMapping(result.getName(), result.getWorkflowNameAsFileName());

        Pair<Boolean, Boolean> noteInfo = calculateNoteInfo(request, notes);

        clearSessionAttributes(request.getSession());

        String successUrl = ServletMapping.EXPORT_WORKFLOW_SUCCESS.getPath() + "?"
                + WF_NAME_FIELD_NAME + "=" + JiraUrlCodec.encode(wfName)
                + "&" + WF_MODE_FIELD_NAME + "=" + wfMode
                + "&" + EXPORTED_FILE + "=" + result.getName()
                + "&" + EXPORTED_FILE_NAME + "=" + JiraUrlCodec.encode(result.getWorkflowNameAsFileName())
                + "&" + MANUAL_NOTES + "=" + noteInfo.first()
                + "&" + GENERATED_NOTES + "=" + noteInfo.second();

        response.sendRedirect(successUrl);
    }

    private void showSuccess(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws DataNotFoundInSessionException, ValidationException, IOException
    {
        String exportedFile = request.getParameter(EXPORTED_FILE);

        File zipFile = fileManager.getExportedWorkflowFile(exportedFile);
        if (!zipFile.exists())
        {
            long intervalInMinutes = fileCleaningJobScheduler.getCurrentMaxAge();
            String error = i18n.getText("wfshare.export.file.not.found", intervalInMinutes);

            errorRenderer.render(error, response, context);

            return;
        }

        String wfName = request.getParameter(WF_NAME_FIELD_NAME);
        String wfMode = request.getParameter(WF_MODE_FIELD_NAME);

        String exportedFileName = request.getParameter(EXPORTED_FILE_NAME);

        boolean manualNotes = Boolean.valueOf(request.getParameter(MANUAL_NOTES));
        boolean generatedNotes = Boolean.valueOf(request.getParameter(GENERATED_NOTES));

        context.put("workflowName", exportedFileName);
        context.put("exportedZip", exportedFile);

        context.put("manualNotes", manualNotes);
        context.put("generatedNotes", generatedNotes);

        context.put("nrOfPlugins", getNrOfPlugins(wfName, wfMode));

        context.put("maxAgeInMinutes", fileCleaningJobScheduler.getCurrentMaxAge());

        String cancelUrl = (String) context.get("cancelUrl");
        context.put("cancelUrlEncoded", encodeUrl(cancelUrl));

        context.put("helpPath", helpPathResolver.getHelpPath("workflow_sharing"));

        renderer.render(ServletMapping.EXPORT_WORKFLOW_SUCCESS.getResultTemplate(), context, response.getWriter());
    }

    private <T> T getSessionAttribute(HttpServletRequest request, String name) throws DataNotFoundInSessionException
    {
        T value = getSessionAttributeOrNull(request, name);

        if (value != null)
        {
            return value;
        }

        throw new DataNotFoundInSessionException();
    }

    private Pair<Boolean, Boolean> calculateNoteInfo(HttpServletRequest request, String notes)
    {
        boolean manualNotes;
        boolean generatedNotes;

        String generatedNotesHash = getSessionAttributeOrNull(request, SessionVar.GENERATED_NOTES_HASH.name());

        if (generatedNotesHash != null)
        {
            generatedNotes = true;

            if (StringUtils.isEmpty(notes))
            {
                manualNotes = true;
            }
            else
            {
                notes = notes.replace("\r", "");
                String notesHash = md5Hex(notes);

                manualNotes = !generatedNotesHash.equals(notesHash);
            }
        }
        else
        {
            generatedNotes = false;
            manualNotes = StringUtils.isNotEmpty(notes);
        }

        return Pair.strictPairOf(manualNotes, generatedNotes);
    }

    private int getNrOfPlugins(String wfName, String wfMode) throws ValidationException
    {
        JiraWorkflow workflow = getWorkflow(wfName, wfMode);

        Multimap<Plugin, String> requiredPlugins = workflowExtensionsHelper.getRequiredPlugins(workflow);

        return requiredPlugins.size();
    }

    private JiraWorkflow getWorkflow(String wfName, String wfMode) throws ValidationException
    {
        JiraWorkflow workflow = workflowExtensionsHelper.getWorkflowForNameAndMode(wfName, wfMode);

        if (null == workflow)
        {
            throw new ValidationException(i18n.getText("wfshare.exception.workflow.not.found", wfName));
        }

        return workflow;
    }

    private Pair<String, String> extractWorkflowNameAndModeFromSession(HttpServletRequest request) throws DataNotFoundInSessionException
    {
        String wfName = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFNAME.name());
        String wfMode = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFMODE.name());

        return Pair.strictPairOf(wfName, wfMode);
    }
    
    private Pair<String,String> extractWorkflowNameAndMode(HttpServletRequest request)
    {
        String wfName = request.getParameter(WF_NAME_FIELD_NAME);
        String wfMode = request.getParameter(WF_MODE_FIELD_NAME);

        if(StringUtils.isBlank(wfName))
        {
            return null;
        }

        if(StringUtils.isBlank(wfMode))
        {
            wfMode = "live";//NON-NLS
        }
        
        return Pair.strictPairOf(wfName, wfMode);
    }

    private void setNavigationPaths(Map<String, Object> context, ServletMapping servletMapping)
    {
        String nextPath = "/plugins/servlet/wfshare-export/" + servletMapping.getPath();//NON-NLS

        String backPath = "";
        if (null != servletMapping.previous())
        {
            backPath = "/plugins/servlet/wfshare-export/" + servletMapping.previous().getPath();//NON-NLS
        }

        context.put("nextUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + nextPath);//NON-NLS
        context.put("backUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + backPath);//NON-NLS
    }

    @Override
    protected void clearSessionAttributes(HttpSession session)
    {
        super.clearSessionAttributes(session);
        for (SessionVar sessionVar : SessionVar.values())
        {
            session.removeAttribute(sessionVar.name());
        }
    }
}
