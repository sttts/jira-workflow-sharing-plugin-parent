package com.atlassian.jira.plugins.workflow.sharing.file;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class FileCleaningJobScheduler implements LifecycleAware, DisposableBean
{
    private final Logger LOG = LoggerFactory.getLogger(FileCleaningJobScheduler.class);

    private static final long DEFAULT_INTERVAL = 60;

    private final FileManager fileManager;

    private long currentMaxAge = DEFAULT_INTERVAL;

    private ScheduledExecutorService scheduler;
    private ScheduledFuture<?> currentTask;

    public FileCleaningJobScheduler(FileManager fileManager)
    {
        this.fileManager = fileManager;
    }

    @Override
    public void onStart()
    {
        scheduler = Executors.newScheduledThreadPool(1,
                new ThreadFactoryBuilder()
                        .setDaemon(true)
                        .setNameFormat("Workflow Sharing Cleanup-%d")
                        .build());

        schedule();
    }

    @Override
    public void destroy()
    {
        unschedule();

        scheduler.shutdown();
    }

    public void reschedule(long maxAge)
    {
        unschedule();

        currentMaxAge = maxAge;

        schedule();
    }

    private void schedule()
    {
        LOG.debug("Scheduling deletion of old workflow files to " + currentMaxAge + " minutes");

        currentTask = scheduler.scheduleAtFixedRate(new FileCleaningJob(), currentMaxAge, currentMaxAge, TimeUnit.MINUTES);
    }

    private void unschedule()
    {
        if (currentTask != null)
        {
            currentTask.cancel(true);
            currentTask = null;
        }
    }

    public long getCurrentMaxAge()
    {
        return currentMaxAge;
    }

    public class FileCleaningJob implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                long maxAgeInMs = currentMaxAge * 60 * 1000;

                fileManager.clearOlderThan(maxAgeInMs);
            }
            catch (Exception e)
            {
                LOG.error("Error occurred while cleaning workflow exports.", e);
            }
        }
    }
}
