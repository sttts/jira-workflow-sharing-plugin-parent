package com.atlassian.jira.plugins.workflow.sharing.servlet;

import com.atlassian.jira.config.properties.ApplicationProperties;

import javax.servlet.http.HttpServlet;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public abstract class AbstractServlet extends HttpServlet
{
    private final ApplicationProperties applicationProperties;

    protected AbstractServlet(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    protected String encodeUrl(String value) throws UnsupportedEncodingException
    {
        return URLEncoder.encode(value, applicationProperties.getEncoding());
    }
}
