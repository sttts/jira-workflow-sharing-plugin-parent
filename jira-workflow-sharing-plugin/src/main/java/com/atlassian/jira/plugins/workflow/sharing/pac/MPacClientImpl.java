package com.atlassian.jira.plugins.workflow.sharing.pac;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PluginDetailQuery;
import com.atlassian.marketplace.client.api.PluginQuery;
import com.atlassian.marketplace.client.api.Plugins;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.marketplace.client.model.PluginSummary;
import com.atlassian.upm.api.util.Option;
import org.apache.commons.lang.exception.NestableRuntimeException;

public class MPacClientImpl implements JWSPacClient
{
    private static final String WORKFLOW_BUNDLE_CATEGORY = "Workflow Bundles";

    private final MarketplaceClient marketplaceClient;

    public MPacClientImpl(MarketplaceClient marketplaceClient)
    {
        this.marketplaceClient = marketplaceClient;
    }

    @Override
    public Page<PluginSummary> getWorkflowBundleList(int offset)
    {
        try
        {
            Plugins pluginResource = marketplaceClient.plugins();

            PluginQuery query = PluginQuery.builder()
                                                .application(Option.option(ApplicationKey.JIRA))
                                                .category(WORKFLOW_BUNDLE_CATEGORY)
                                                .view(Option.option(PluginQuery.View.POPULAR))
                                                .offset(offset)
                                                .build();
            return pluginResource.find(query);
        }
        catch (Exception e)
        {
            throw new NestableRuntimeException(e);
        }
    }

    @Override
    public Plugin getWorkflowBundleDetails(String pluginKey)
    {
        try
        {
            Plugins pluginResource = marketplaceClient.plugins();
            PluginDetailQuery detailQuery = PluginDetailQuery.builder(pluginKey).application(Option.option(ApplicationKey.JIRA)).build();
    
            Option<Plugin> pluginOption = pluginResource.get(detailQuery);
            if (pluginOption.isDefined())
            {
                return pluginOption.get();
            }

            return null;
        }
        catch (Exception e)
        {
            throw new NestableRuntimeException(e);
        }
    }
}
