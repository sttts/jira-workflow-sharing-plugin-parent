package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.sal.api.message.I18nResolver;

import java.util.Collection;

public class ModuleDescriptorLocatorImpl implements ModuleDescriptorLocator
{
    private final PluginAccessor pluginAccessor;
    private final I18nResolver i18n;
    private ModuleDescriptorFactory moduleDescriptorFactory;

    public ModuleDescriptorLocatorImpl(PluginAccessor pluginAccessor, I18nResolver i18n, ModuleDescriptorFactory moduleDescriptorFactory)
    {
        this.pluginAccessor = pluginAccessor;
        this.i18n = i18n;
        this.moduleDescriptorFactory = moduleDescriptorFactory;
    }

    @Override
    public Collection<ModuleDescriptor> getEnabledModuleDescriptorsByModuleClassname(String moduleClassname)
    {
        return getEnabledModuleDescriptors(new ModuleOfClassnamePredicate(moduleClassname));
    }

    @Override
    public Collection<ModuleDescriptor> getEnabledModuleDescriptorsByModuleType(String moduleType)
    {
        return getEnabledModuleDescriptors(new ModuleDescriptorOfTypePredicate(moduleDescriptorFactory, moduleType));
    }

    private Collection<ModuleDescriptor> getEnabledModuleDescriptors(final ModuleDescriptorPredicate<ModuleDescriptor> predicate)
    {
        final EnabledModulePredicate enabled = new EnabledModulePredicate(pluginAccessor);

        return pluginAccessor.getModuleDescriptors(new ModuleDescriptorPredicate()
        {
            public boolean matches(final ModuleDescriptor moduleDescriptor)
            {
                return predicate.matches(moduleDescriptor) && enabled.matches(moduleDescriptor);
            }
        });
    }

    public class ModuleOfClassnamePredicate implements ModuleDescriptorPredicate
    {
        private final String moduleClassname;

        /**
         * @throws IllegalArgumentException if the moduleClass is <code>null</code>
         */
        public ModuleOfClassnamePredicate(final String moduleClassname)
        {
            if (moduleClassname == null)
            {
                throw new IllegalArgumentException(i18n.getText("wfshare.exception.module.classname.should.not.be.null"));
            }
            this.moduleClassname = moduleClassname;
        }

        public boolean matches(final ModuleDescriptor moduleDescriptor)
        {
            if (moduleDescriptor != null)
            {
                if(AbstractWorkflowModuleDescriptor.class.isAssignableFrom(moduleDescriptor.getClass()) && !moduleDescriptor.getPluginKey().equals("com.atlassian.jira.plugin.system.workflow"))//NON-NLS
                {
                    AbstractWorkflowModuleDescriptor wfModuleDescriptor = (AbstractWorkflowModuleDescriptor) moduleDescriptor;

                    final Class<?> workflowClassInDescriptor = wfModuleDescriptor.getImplementationClass();
                    return (workflowClassInDescriptor != null) && workflowClassInDescriptor.getName().equals(moduleClassname);
                }
                else if(!AbstractWorkflowModuleDescriptor.class.isAssignableFrom(moduleDescriptor.getClass()))
                {
                    final Class<?> moduleClassInDescriptor = moduleDescriptor.getModuleClass();
                    return (null != moduleClassInDescriptor && moduleClassInDescriptor.getName().equals(moduleClassname));
                }
            }

            return false;
        }
    }
}
