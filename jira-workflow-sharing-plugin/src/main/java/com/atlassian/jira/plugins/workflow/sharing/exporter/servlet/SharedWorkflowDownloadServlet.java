package com.atlassian.jira.plugins.workflow.sharing.exporter.servlet;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.workflow.sharing.file.FileCleaningJobScheduler;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManager;
import com.atlassian.jira.plugins.workflow.sharing.servlet.AbstractServlet;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class SharedWorkflowDownloadServlet extends AbstractServlet
{
    private static final String ORIGIN_PARAM = "origin";

    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final FileManager fileManager;
    private final I18nResolver i18n;
    private final ErrorRenderer errorRenderer;
    private final FileCleaningJobScheduler fileCleaningJobScheduler;

    public SharedWorkflowDownloadServlet(UserManager userManager, LoginUriProvider loginUriProvider,
                                         FileManager fileManager, I18nResolver i18n, ErrorRenderer errorRenderer,
                                         FileCleaningJobScheduler fileCleaningJobScheduler, ApplicationProperties applicationProperties)
    {
        super(applicationProperties);

        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.fileManager = fileManager;
        this.i18n = i18n;
        this.errorRenderer = errorRenderer;
        this.fileCleaningJobScheduler = fileCleaningJobScheduler;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String username = userManager.getRemoteUsername(req);
        if (username == null || (!userManager.isAdmin(username) && !userManager.isSystemAdmin(username)))
        {
            redirectToLogin(req, resp);
            return;
        }

        InputStream is = null;
        OutputStream sos = null;

        try
        {
            String actualFilename = req.getParameter("file");

            String nameToDisplay = fileManager.getExportFileNameToDisplay(actualFilename);

            File zipFile = fileManager.getExportedWorkflowFile(actualFilename);
            if (!zipFile.exists())
            {
                long intervalInMinutes = fileCleaningJobScheduler.getCurrentMaxAge();
                String error = i18n.getText("wfshare.export.file.not.found", intervalInMinutes);

                renderError(req, resp, error);

                return;
            }

            is = new FileInputStream(zipFile);

            resp.setContentType("application/zip");//NON-NLS
            resp.setHeader("Content-Disposition", "attachment; filename=\"" + nameToDisplay + "\"");

            sos = resp.getOutputStream();
            IOUtils.copy(is, sos);

            sos.flush();
        }
        catch (FileNotFoundException e)
        {
            String error = i18n.getText("wfshare.export.could.not.read.file");

            renderError(req, resp, error);
        }
        finally
        {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(sos);
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private void renderError(HttpServletRequest req, HttpServletResponse resp, String error) throws IOException
    {
        resp.setContentType("text/html;charset=utf-8");

        Map<String, Object> context = new HashMap<String, Object>();
        String originalUrl = req.getParameter(ORIGIN_PARAM);
        context.put("cancelUrl", (originalUrl == null) ? "" : encodeUrl(originalUrl));

        errorRenderer.render(error, resp, context);
    }
}
