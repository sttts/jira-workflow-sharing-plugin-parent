package com.atlassian.jira.plugins.workflow.sharing.util;

import java.io.Closeable;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LimitedInputStream extends FilterInputStream implements Closeable
{
    private final long maxSize;
    private long currentSize;

    public LimitedInputStream(InputStream stream, long maxSize)
    {
        super(stream);
        this.maxSize = maxSize;
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException
    {
        checkSize();
        int size = super.read(b, off, len);
        if (size > 0)
        {
            currentSize += size;
            checkSize();
        }
        return size;
    }

    @Override
    public int read() throws IOException
    {
        checkSize();
        int data = super.read();
        if (data >= 0)
        {
            currentSize++;
            checkSize();
        }
        return data;
    }

    @Override
    public long skip(final long n) throws IOException
    {
        checkSize();
        final long skip = super.skip(n);
        currentSize += skip;
        checkSize();
        return skip;
    }

    private void checkSize() throws IOException
    {
        if (currentSize > maxSize)
        {
            throw new StreamTooBigException(currentSize, maxSize);
        }
    }

    public static class StreamTooBigException extends IOException
    {
        private final long size;
        private final long maxSize;

        public StreamTooBigException(long size, long maxSize)
        {
            this.size = size;
            this.maxSize = maxSize;
        }

        public long getSize()
        {
            return size;
        }

        public long getMaxSize()
        {
            return maxSize;
        }
    }
}
