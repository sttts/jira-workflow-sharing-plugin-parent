package com.atlassian.jira.plugins.workflow.sharing.importer;

import java.io.Serializable;
import java.util.Comparator;

public class StatusMapping implements Serializable
{
    public static Comparator<StatusMapping> OLD_NAME_ORDER = new Comparator<StatusMapping>()
    {
        @Override
        public int compare(StatusMapping o1, StatusMapping o2)
        {
            return o1.getOriginalName().compareToIgnoreCase(o2.getOriginalName());
        }
    };

    private static final long serialVersionUID = -3138129003596652280L;
    private String originalId;
    private String originalName;
    private String newName;
    private String newId;

    public StatusMapping(String originalId, String originalName, String newId, String newName)
    {
        this.originalId = originalId;
        this.originalName = originalName;
        this.newName = newName;
        this.newId = newId;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    public String getOriginalName()
    {
        return originalName;
    }

    public String getNewName()
    {
        return newName;
    }

    public String getNewId()
    {
        return newId;
    }

    public void setNewId(String id)
    {
        this.newId = id;
    }

    public boolean isNewStatus()
    {
        return "-1".equals(newId);
    }
}
