package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.WorkflowSharingFiles;
import com.atlassian.jira.plugins.workflow.sharing.importer.ImporterConstants;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.jira.plugins.workflow.sharing.util.IOSupport;
import com.atlassian.jira.plugins.workflow.sharing.util.LimitedInputStream;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

public class BundleParser
{
    private final Logger log = LoggerFactory.getLogger(BundleParser.class);

    private final ErrorMessageProvider errorMessageProvider;
    private final List<EntryHandler> handlerList;
    private final long maxStream;
    private final long maxEntry;

    public BundleParser(ErrorMessageProvider errorMessageProvider)
    {
        this(errorMessageProvider, ImporterConstants.MAX_STREAM, ImporterConstants.MAX_ENTRY_BYTES);
    }

    public BundleParser(ErrorMessageProvider errorMessageProvider, long maxStream, long maxEntry)
    {
        this.errorMessageProvider = errorMessageProvider;
        ObjectMapper mapper = new ObjectMapper();
        mapper.getJsonFactory().configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false);
        this.handlerList = ImmutableList.of(new WorkflowHandler(), new AnnotationsHandler(), new LayoutHandler(),
                new NotesHandler(), new ScreensHandler(mapper), new CustomFieldHandler(mapper),
                new PluginsFieldHandler(mapper), new PluginsDirHandler());

        this.maxStream = maxStream;
        this.maxEntry = maxEntry;
    }

    public Builder parse(InputStream stream, boolean ignoreUnknownFiles) throws IOException, ValidationException
    {
        ZipInputStream zis = null;
        ZipEntry entry;
        try
        {
            zis = new ZipInputStream(new IgnoreCloseInputStream(new LimitedInputStream(stream, maxStream)));
            entry = zis.getNextEntry();
        }
        catch (ZipException exception)
        {
            IOUtils.closeQuietly(zis);
            throw new ValidationException(errorMessageProvider.notZip());
        }
        catch (LimitedInputStream.StreamTooBigException e)
        {
            throw new ValidationException(errorMessageProvider.bundleEntryTooBig(e.getSize(), e.getMaxSize()));
        }

        if (entry == null)
        {
            throw new ValidationException(errorMessageProvider.notZip());
        }

        Builder builder = new Builder();
        try
        {
            do
            {
                if (entry.getSize() > 0 && entry.getSize() > maxEntry)
                {
                    throw new ValidationException(errorMessageProvider.bundleEntryTooBig(entry.getSize(), maxEntry));
                }

                boolean handled = false;
                try
                {
                    LimitedInputStream limitedStream = new LimitedInputStream(zis, maxEntry);
                    for (EntryHandler entryHandler : handlerList)
                    {
                        if (entryHandler.handle(entry, limitedStream, builder, errorMessageProvider))
                        {
                            handled = true;
                            break;
                        }
                    }
                }
                catch (LimitedInputStream.StreamTooBigException e)
                {
                    throw new ValidationException(errorMessageProvider.bundleEntryTooBig(e.getSize(), e.getMaxSize()));
                }
                if (!handled)
                {
                    if (!ignoreUnknownFiles)
                    {
                        throw new ValidationException(errorMessageProvider.unknownFile(entry.getName()));
                    }

                    if (log.isDebugEnabled())
                    {
                        log.debug("Ignoring zip entry '{}'.", entry);
                    }
                }
            }
            while ((entry = zis.getNextEntry()) != null);
        }
        catch (LimitedInputStream.StreamTooBigException e)
        {
            throw new ValidationException(errorMessageProvider.bundleEntryTooBig(e.getSize(), e.getMaxSize()));
        }
        finally
        {
            IOUtils.closeQuietly(zis);
        }

        if (builder.getWorkflowXml() == null)
        {
            throw new ValidationException(errorMessageProvider.noWorkflowXml());
        }

        return builder;
    }

    public interface ErrorMessageProvider
    {
        String notZip();

        String bundleEntryTooBig(long size, long maxSize);

        String unknownFile(String fileName);

        String noWorkflowXml();

        String invalidJson(String fileName);
    }

    static class Builder
    {
        private WorkflowBundle.BundleSource source;
        private String workflowXml;
        private String layout;
        private String annotations;
        private List<WorkflowExtensionsPluginInfo> pluginInfo = ImmutableList.of();
        private List<CustomFieldInfo> customFieldInfos = ImmutableList.of();
        private List<ScreenInfo> screenInfoList = ImmutableList.of();
        private String notes;

        public Builder setAnnotations(final String annotations)
        {
            this.annotations = annotations;
            return this;
        }

        public Builder setCustomFieldInfos(final List<CustomFieldInfo> customFieldInfos)
        {
            this.customFieldInfos = customFieldInfos;
            return this;
        }

        public Builder setPluginInfo(final List<WorkflowExtensionsPluginInfo> pluginInfo)
        {
            this.pluginInfo = pluginInfo;
            return this;
        }

        public Builder setLayout(final String layout)
        {
            this.layout = layout;
            return this;
        }

        public Builder setNotes(final String notes)
        {
            this.notes = notes;
            return this;
        }

        public Builder setScreenInfoList(final List<ScreenInfo> screenInfoList)
        {
            this.screenInfoList = screenInfoList;
            return this;
        }

        public Builder setSource(final WorkflowBundle.BundleSource source)
        {
            this.source = source;
            return this;
        }

        public Builder setWorkflowXml(final String workflowXml)
        {
            this.workflowXml = workflowXml;
            return this;
        }

        public String getAnnotations()
        {
            return annotations;
        }

        public List<CustomFieldInfo> getCustomFieldInfos()
        {
            return customFieldInfos;
        }

        public List<WorkflowExtensionsPluginInfo> getPluginInfo()
        {
            return pluginInfo;
        }

        public String getLayout()
        {
            return layout;
        }

        public String getNotes()
        {
            return notes;
        }

        public List<ScreenInfo> getScreenInfoList()
        {
            return screenInfoList;
        }

        public WorkflowBundle.BundleSource getSource()
        {
            return source;
        }

        public String getWorkflowXml()
        {
            return workflowXml;
        }

        public DefaultWorkflowBundle build()
        {
            return new DefaultWorkflowBundle(this);
        }
    }

    private static class DefaultWorkflowBundle implements WorkflowBundle
    {
        private final BundleSource source;
        private final String workflowXml;
        private final String layout;
        private final String annotations;
        private final List<WorkflowExtensionsPluginInfo> pluginInfo;
        private final List<CustomFieldInfo> customFieldInfo;
        private final List<ScreenInfo> screenInfo;
        private final String notes;

        private DefaultWorkflowBundle(Builder builder)
        {
            this.annotations = builder.getAnnotations();
            this.source = builder.getSource();
            this.workflowXml = builder.getWorkflowXml();
            this.layout = builder.getLayout();
            this.notes = builder.getNotes();

            this.pluginInfo = uniqueList(builder.getPluginInfo(), WorkflowExtensionsPluginInfo.GET_KEY);
            this.customFieldInfo = uniqueList(builder.getCustomFieldInfos(), CustomFieldInfo.GET_ORIGINAL_ID);
            this.screenInfo = uniqueList(builder.getScreenInfoList(), ScreenInfo.GET_ORIGINAL_ID);
        }

        @Override
        public BundleSource getSource()
        {
            return source;
        }

        @Override
        public String getWorkflowXml()
        {
            return workflowXml;
        }

        @Override
        public String getLayout()
        {
            return layout;
        }

        @Override
        public String getAnnotations()
        {
            return annotations;
        }

        @Override
        public Iterable<WorkflowExtensionsPluginInfo> getPluginInfo()
        {
            return pluginInfo;
        }

        @Override
        public Iterable<CustomFieldInfo> getCustomFieldInfo()
        {
            return customFieldInfo;
        }

        @Override
        public Iterable<ScreenInfo> getScreenInfo()
        {
            return screenInfo;
        }

        @Override
        public String getNotes()
        {
            return notes;
        }
    }

    private interface EntryHandler
    {
        boolean handle(ZipEntry entry, InputStream input, Builder builder, ErrorMessageProvider errorMessageProvider) throws IOException, ValidationException;
    }

    private static class PluginsDirHandler implements EntryHandler
    {
        @Override
        public boolean handle(ZipEntry entry, InputStream input, Builder builder, ErrorMessageProvider errorMessageProvider) throws IOException, ValidationException
        {
            return entry.getName().startsWith(WorkflowSharingFiles.PLUGINS_DIR.getPath());
        }
    }

    private abstract static class StringEntryHandler implements EntryHandler
    {
        private final WorkflowSharingFiles file;

        private StringEntryHandler(final WorkflowSharingFiles file)
        {
            this.file = file;
        }

        @Override
        public boolean handle(final ZipEntry entry, final InputStream input, final Builder builder, ErrorMessageProvider errorMessageProvider)
                throws IOException
        {
            if (entry.getName().equals(file.getPath()))
            {
                final String str = IOSupport.readString(input);
                if (str != null)
                {
                    addString(StringUtils.stripToNull(str), builder);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        abstract void addString(String string, Builder builder);
    }

    private static class AnnotationsHandler extends StringEntryHandler
    {
        private AnnotationsHandler()
        {
            super(WorkflowSharingFiles.ANNOTATION);
        }

        @Override
        void addString(final String string, final Builder builder)
        {
            builder.setAnnotations(string);
        }
    }

    private static class LayoutHandler extends StringEntryHandler
    {
        private LayoutHandler()
        {
            super(WorkflowSharingFiles.LAYOUT);
        }

        @Override
        void addString(final String string, final Builder builder)
        {
            builder.setLayout(string);
        }
    }

    private static class NotesHandler extends StringEntryHandler
    {
        private NotesHandler()
        {
            super(WorkflowSharingFiles.NOTES);
        }

        @Override
        void addString(final String string, final Builder builder)
        {
            builder.setNotes(string);
        }
    }

    private static class WorkflowHandler extends StringEntryHandler
    {
        private WorkflowHandler()
        {
            super(WorkflowSharingFiles.WORKFLOW);
        }

        @Override
        void addString(final String string, final Builder builder)
        {
            builder.setWorkflowXml(string);
        }
    }

    private abstract static class JsonHandler<T> implements EntryHandler
    {
        private final WorkflowSharingFiles file;
        private final TypeReference<T> type;
        private final ObjectMapper mapper;

        private JsonHandler(final WorkflowSharingFiles file, final TypeReference<T> type, ObjectMapper mapper)
        {
            this.file = file;
            this.type = type;
            this.mapper = mapper;
        }

        @Override
        public boolean handle(final ZipEntry entry, final InputStream input, final Builder builder, ErrorMessageProvider errorMessageProvider)
                throws IOException, ValidationException
        {
            if (!file.getPath().equals(entry.getName()))
            {
                return false;
            }

            final T o;
            try
            {
                o = mapper.readValue(input, type);
            }
            catch (JsonProcessingException e)
            {
                throw new ValidationException(errorMessageProvider.invalidJson(entry.getName()));
            }

            if (o != null)
            {
                addObject(o, builder);
            }
            return true;
        }
        abstract void addObject(T o, Builder builder);
    }

    private static class ScreensHandler extends JsonHandler<List<ScreenInfo>>
    {
        private ScreensHandler(ObjectMapper mapper)
        {
            super(WorkflowSharingFiles.SCREENS, ScreenInfo.LIST_TYPE, mapper);
        }

        @Override
        void addObject(final List<ScreenInfo> o, final Builder builder)
        {
            builder.setScreenInfoList(ImmutableList.copyOf(o));
        }
    }

    private static class CustomFieldHandler extends JsonHandler<List<CustomFieldInfo>>
    {
        private CustomFieldHandler(ObjectMapper mapper )
        {
            super(WorkflowSharingFiles.CUSTOM_FIELDS, CustomFieldInfo.LIST_TYPE, mapper);
        }

        @Override
        void addObject(final List<CustomFieldInfo> o, final Builder builder)
        {
            builder.setCustomFieldInfos(ImmutableList.copyOf(o));
        }
    }

    private static class PluginsFieldHandler extends JsonHandler<List<WorkflowExtensionsPluginInfo>>
    {
        private PluginsFieldHandler(ObjectMapper mapper)
        {
            super(WorkflowSharingFiles.PLUGINS, WorkflowExtensionsPluginInfo.LIST_TYPE, mapper);
        }

        @Override
        void addObject(final List<WorkflowExtensionsPluginInfo> o, final Builder builder)
        {
            builder.setPluginInfo(ImmutableList.copyOf(o));
        }
    }

    private static final class IgnoreCloseInputStream extends FilterInputStream
    {
        protected IgnoreCloseInputStream(InputStream in)
        {
            super(in);
        }

        @Override
        public void close()
        {
        }
    }

    private static <T> List<T> uniqueList(Iterable<T> items, Function<? super T, ?> toKey)
    {
        if (items == null)
        {
            return ImmutableList.of();
        }
        else
        {
            ImmutableList.Builder<T> builder = ImmutableList.builder();
            Set<Object> object = Sets.newHashSet();
            for (T item : items)
            {
                if (object.add(toKey.apply(item)))
                {
                    builder.add(item);
                }
            }
            return builder.build();
        }
    }

    public long getMaxStream()
    {
        return maxStream;
    }
}
