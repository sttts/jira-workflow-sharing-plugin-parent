package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.sysbliss.jira.plugins.workflow.util.StatusUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.*;

public class WorkflowStatusHelperImpl implements WorkflowStatusHelper
{
    private final ConstantsManager constantsManager;
    private final I18nResolver i18n;
    private final ValidNameGenerator validNameGenerator;

    public WorkflowStatusHelperImpl(ConstantsManager constantsManager, I18nResolver i18n, ValidNameGenerator validNameGenerator)
    {
        this.constantsManager = constantsManager; 
        this.i18n = i18n;
        this.validNameGenerator = validNameGenerator;
    }

    @Override
    public Map<String,StatusMapping> getStatusHolders(String xml)
    {
        Collection<Status> jiraStatuses = constantsManager.getStatusObjects();

        Map<String,StatusMapping> holders = new HashMap<String, StatusMapping>();

        WorkflowDescriptor descriptor;
        try
        {
            descriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(xml);
        }
        catch (FactoryException e)
        {
            throw new RuntimeException(e);
        }

        List<StepDescriptor> steps = descriptor.getSteps();

        for (StepDescriptor step : steps)
        {
            if ((step.getMetaAttributes() != null) && step.getMetaAttributes().containsKey(JiraWorkflow.STEP_STATUS_KEY))
            {
                String statusId = (String) step.getMetaAttributes().get(JiraWorkflow.STEP_STATUS_KEY);
                Status found = Iterables.find(jiraStatuses, new StatusNamePredicate(step.getName()), null);
                if (found != null)
                {
                    holders.put(statusId, new StatusMapping(statusId, step.getName(), found.getId(), getValidStatusName(found.getName())));
                }
                else
                {
                    holders.put(statusId, new StatusMapping(statusId, step.getName(), "-1", getValidStatusName(step.getName())));
                }
            }
        }

        return holders;
    }

    @Override
    public List<Status> getJiraStatuses()
    {
        return ImmutableList.copyOf(constantsManager.getStatusObjects());
    }

    @Override
    public void removeStatus(String id) throws GenericEntityException
    {
        StatusUtils statusUtils = new StatusUtils();
        statusUtils.deleteStatus(id);
    }

    @Override
    public Status createStatus(String newName, String newStatusDefaultIcon) throws GenericEntityException
    {
        String validName = getValidStatusName(newName);
        StatusUtils statusUtils = new StatusUtils();
        GenericValue gv = statusUtils.addStatus(validName, "", newStatusDefaultIcon);
        return constantsManager.getStatusObject(gv.getString("id"));//NON-NLS
    }

    @Override
    public String getValidStatusName(String name)
    {
        return validNameGenerator.getValidName(name, 60, new Function<String, Boolean>()
        {
            @Override
            public Boolean apply(String statusName)
            {
                for (Status status : constantsManager.getStatusObjects())
                {
                    if (status.getName().equalsIgnoreCase(statusName))
                    {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    @Override
    public String getNameForStatusId(String id)
    {
        String name = i18n.getText("wfshare.exception.status.not.found");
        
        Status status = constantsManager.getStatusObject(id);
        if(null != status)
        {
            name = status.getName();
        }
        
        return name;
    }

    private class StatusNamePredicate implements Predicate<Status>
    {
        private final String name;

        private StatusNamePredicate(String name)
        {
            this.name = name;
        }

        @Override
        public boolean apply(Status status)
        {
            return name.equalsIgnoreCase(status.getName());
        }
    }
    
}
