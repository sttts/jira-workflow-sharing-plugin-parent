package com.atlassian.jira.plugins.workflow.sharing.pac;

import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.marketplace.client.model.PluginSummary;

public interface JWSPacClient
{
    Page<PluginSummary> getWorkflowBundleList(int offset);

    Plugin getWorkflowBundleDetails(String pluginKey);
}
