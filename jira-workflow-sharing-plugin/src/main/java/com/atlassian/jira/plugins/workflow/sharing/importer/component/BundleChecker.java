package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.*;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.Comparator;
import java.util.List;

public class BundleChecker
{
    public static void main(String[] args)
    {
        if (args.length == 0)
        {
            System.err.println("Bundle file is not specified as first argument");
            return;
        }

        String fileName = args[0];
        File bundleFile = new File(fileName);
        if (!bundleFile.exists())
        {
            System.err.println("File not found: '" + fileName + "'");
            return;
        }

        BundleParser bundleParser = new BundleParser(new SimpleErrorMessageProvider());

        long bundleSize = bundleFile.length();
        long maxSize = bundleParser.getMaxStream();
        if (bundleSize > maxSize)
        {
            System.err.println("Validation error: Bundle is too big: " + bundleSize + ". Max allowed size is " + maxSize);
            return;
        }

        InputStream stream = null;
        try
        {
            stream = new FileInputStream(bundleFile);

            check(stream, bundleParser);
        }
        catch (FileNotFoundException e)
        {
            System.err.println("File not found: '" + fileName + "'");
        }
        finally
        {
            IOUtils.closeQuietly(stream);
        }
    }

    private static void check(InputStream stream, BundleParser bundleParser)
    {
        try
        {
            BundleParser.Builder builder = bundleParser.parse(stream, false);

            validatePlugins(builder);

            validateCustomFields(builder);

            ImportableJiraWorkflow workflow = validateWorkflowXml(builder);
            validateActions(workflow);

            System.out.println("Bundle is OK");
        }
        catch (ValidationException e)
        {
            System.err.println("Validation error: " + e.getMessage());
        }
        catch (IOException e)
        {
            System.err.println("I/O error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void validatePlugins(BundleParser.Builder builder)
    {
        if (CollectionUtils.isNotEmpty(builder.getPluginInfo()))
        {
            System.out.println("Warning: bundle contains plugins");
        }
    }

    private static void validateCustomFields(BundleParser.Builder builder)
    {
        List<CustomFieldInfo> illegal = Lists.newArrayList();
        for (CustomFieldInfo info : builder.getCustomFieldInfos())
        {
            String fieldPluginKey = StringUtils.stripToEmpty(info.getPluginKey());
            if (!fieldPluginKey.startsWith("com.atlassian.jira.plugin.system."))
            {
                illegal.add(info);
            }
        }

        if (!illegal.isEmpty())
        {
            illegal = Ordering.from(new Comparator<CustomFieldInfo>()
            {
                @Override
                public int compare(CustomFieldInfo field1, CustomFieldInfo field2)
                {
                    return field1.getName().compareToIgnoreCase(field2.getName());
                }
            }).sortedCopy(illegal);

            System.out.println("Warning: the following custom fields are not allowed to be imported: ");
            for (CustomFieldInfo field : illegal)
            {
                System.out.println("\t" + field.getName() + " - " + field.getTypeModuleKey());
            }
        }
    }

    private static ImportableJiraWorkflow validateWorkflowXml(BundleParser.Builder builder) throws ValidationException
    {
        try
        {
            WorkflowDescriptor workflowDescriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(builder.getWorkflowXml());

            return new ImportableJiraWorkflow("", workflowDescriptor, null);
        }
        catch (FactoryException e)
        {
            throw new ValidationException("Workflow XML is invalid");
        }
    }

    private static void validateActions(ImportableJiraWorkflow workflow) throws IOException
    {
        WhitelistCheckerImpl whitelistChecker = new WhitelistCheckerImpl();
        whitelistChecker.afterPropertiesSet();

        WorkflowExtensionsHelper helper = new WorkflowExtensionsHelperImpl(null, null, null, null, null, whitelistChecker);

        RemovedItems removedItems = helper.getRemovedItems(workflow, false);

        if (removedItems.isEmpty())
        {
            return;
        }

        System.out.println("Warning: bundle contains the following conditions/validators/post-functions which are not allowed to be imported:");

        for (RemovedItems.ActionRemovedItems actionRemovedItems : removedItems.getActionRemovedItems())
        {
            if (!actionRemovedItems.isEmpty())
            {
                System.out.println("\t" + actionRemovedItems.getAction().getName());

                for (String conditionClass : Ordering.natural().sortedCopy(actionRemovedItems.getConditions()))
                {
                    System.out.println("\t\t" + conditionClass);
                }

                for (String validatorClass : Ordering.natural().sortedCopy(actionRemovedItems.getValidators()))
                {
                    System.out.println("\t\t" + validatorClass);
                }

                for (String functionClass : Ordering.natural().sortedCopy(actionRemovedItems.getFunctions()))
                {
                    System.out.println("\t\t" + functionClass);
                }
            }
        }
    }

    private static class SimpleErrorMessageProvider implements BundleParser.ErrorMessageProvider
    {
        @Override
        public String notZip()
        {
            return "Not a zip file";
        }

        @Override
        public String bundleEntryTooBig(long size, long maxSize)
        {
            return "Bundle file contains an entry which is too big: " + size + ". Max allowed size is " + maxSize;
        }

        @Override
        public String unknownFile(String fileName)
        {
            return "Bundle contains an unknown file: " + fileName;
        }

        @Override
        public String invalidJson(String fileName)
        {
            return "Bundle contains an invalid JSON file: " + fileName;
        }

        @Override
        public String noWorkflowXml()
        {
            return "Workflow XML not found in bundle";
        }
    }
}
