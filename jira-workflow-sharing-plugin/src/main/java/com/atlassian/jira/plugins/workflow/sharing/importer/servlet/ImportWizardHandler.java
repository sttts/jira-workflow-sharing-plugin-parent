package com.atlassian.jira.plugins.workflow.sharing.importer.servlet;

import com.atlassian.gzipfilter.org.apache.commons.lang.WordUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.plugins.workflow.sharing.importer.ImporterConstants;
import com.atlassian.jira.plugins.workflow.sharing.importer.JiraWorkflowSharingImporter;
import com.atlassian.jira.plugins.workflow.sharing.importer.SharedWorkflowImportPlan;
import com.atlassian.jira.plugins.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.PacBundleDownloader;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.ScreenCreator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowBundle;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowImporterFactory;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowStatusHelper;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.AbstractServletWizardHandler;
import com.atlassian.jira.plugins.workflow.sharing.servlet.DataNotFoundInSessionException;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ServletMapping;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.http.JiraHttpUtils;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

public final class ImportWizardHandler extends AbstractServletWizardHandler
{
    private final Logger LOG = LoggerFactory.getLogger(ImportWizardHandler.class);

    public enum SessionVar
    {
        WFSHARE_IMPORT_PLAN, WFSHARE_WORKFLOW_NAME, WFSHARE_STATUS_HOLDERS, WFSHARE_STATUS_MAPPING, PROJECT_ID, SCHEME_ID
    }

    public static final String WF_NAME_FIELD_NAME = "wfShareWorkflowName";
    public static final String BUNDLE_DOWNLOAD_URL_FIELD_NAME = "wfShareBundleDownloadUrl";
    public static final String BUNDLE_FILENAME_FIELD_NAME = "wfShareBundleFilename";
    public static final String PARAM_STATUS_FOR_PREFIX = "status-for-";
    public static final String PROJECT_ID_PARAM = "projectId";
    public static final String SCHEME_ID_PARAM = "schemeId";

    private final TemplateRenderer renderer;
    private final WorkflowImporterFactory importerFactory;
    private final WorkflowManager workflowManager;
    private final WorkflowStatusHelper workflowStatusHelper;
    private final VelocityRequestContextFactory requestContextFactory;
    private final PacBundleDownloader pacFileDownloader;
    private final ScreenCreator screenCreator;
    private final WorkflowBundle.Factory factory;
    private final WebSudoManager webSudoManager;
    private final ProjectManager projectManager;
    private final PluginAccessor pluginAccessor;

    public ImportWizardHandler(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager, TemplateRenderer renderer,
                               WorkflowImporterFactory importerFactory, WorkflowManager workflowManager, I18nResolver i18n, WorkflowStatusHelper workflowStatusHelper,
                               VelocityRequestContextFactory requestContextFactory, PacBundleDownloader pacFileDownloader,
                               ScreenCreator screenCreator, WorkflowBundle.Factory factory,
                               XsrfTokenValidator xsrfTokenValidator, XsrfTokenGenerator xsrfTokenGenerator, ApplicationProperties applicationProperties,
                               ProjectManager projectManager, PluginAccessor pluginAccessor)
    {
        super(loginUriProvider, userManager, i18n, xsrfTokenValidator, xsrfTokenGenerator, applicationProperties);
        this.webSudoManager = webSudoManager;
        this.renderer = renderer;
        this.importerFactory = importerFactory;
        this.workflowManager = workflowManager;
        this.workflowStatusHelper = workflowStatusHelper;
        this.requestContextFactory = requestContextFactory;
        this.pacFileDownloader = pacFileDownloader;

        this.screenCreator = screenCreator;
        this.factory = factory;
        this.projectManager = projectManager;
        this.pluginAccessor = pluginAccessor;
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response, RequestHandler handler) throws IOException
    {
        if (enforceAdminLogin(request, response))
        {
            return;
        }

        JiraHttpUtils.setNoCacheHeaders(response);

        response.setContentType("text/html;charset=utf-8");

        //make sure login redirect info is cleared
        super.clearSessionAttributes(request.getSession());

        Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());//NON-NLS

        ServletMapping requestMapping = ServletMapping.fromPath(getMappingPath(request));

        context.put("cancelUrl", getCancelUrl(request));

        try
        {
            handler.handle(request, response, context, requestMapping);
        }
        catch (DataNotFoundInSessionException e)
        {
            response.sendRedirect(ServletMapping.IMPORT_CHOOSE_ZIP.getPath() + "?" + DATA_NOT_FOUND_IN_SESSION_PARAM + "=true");
        }
        catch (Exception e)
        {
            //We don't know if this is an expected exception or not (i.e. IO error because client disconnected).
            // So we can't just log it.
            LOG.debug("Unable to import workflow", e);

            String message = e.getMessage();
            if (StringUtils.isBlank(message))
            {
                message = i18n.getText("wfshare.import.error.generic");
            }
            showError(message, request, response, context);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        handleRequest(request, response, new RequestHandler()
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ServletMapping requestMapping)
                    throws DataNotFoundInSessionException, IOException, ServletException, ValidationException
            {
                switch (requestMapping)
                {
                    case IMPORT_CHOOSE_ZIP:
                        showChooseZip(request, response, context, requestMapping);
                        break;
                    case IMPORT_SET_NAME:
                        showChooseName(request, response, context);
                        break;
                    case IMPORT_MAP_STATUSES:
                        showMapStatuses(request, response, context);
                        break;
                    case IMPORT_VIEW_SUMMARY:
                        showSummary(request, response, context);
                        break;
                    case IMPORT_WORKFLOW:
                        doImport(request, response);
                        break;
                    case IMPORT_WORKFLOW_SUCCESS:
                        showSuccess(request, response, context);
                        break;
                }
            }
        });
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        handleRequest(request, response, new RequestHandler()
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ServletMapping requestMapping)
                    throws DataNotFoundInSessionException, IOException, ValidationException, URISyntaxException, ServletException
            {
                switch (requestMapping)
                {
                    case IMPORT_CHOOSE_ZIP:
                        if (!ServletFileUpload.isMultipartContent(request) && null != request.getParameter(BUNDLE_DOWNLOAD_URL_FIELD_NAME))
                        {
                            doDownloadMarketplaceZip(request, response, context);
                        }
                        else if (request.getParameter(WF_NAME_FIELD_NAME) == null)
                        {
                            doUploadZip(request, response, context);
                        }
                        break;
                    case IMPORT_SET_NAME:
                        doSetName(request, response, context);
                        break;
                    case IMPORT_MAP_STATUSES:
                        doMapStatuses(request, response);
                        break;
                    case IMPORT_WORKFLOW:
                        doImport(request, response);
                        break;
                }
            }
        });
    }

    private void showError(String error, HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException
    {
        String errorTemplate = ServletMapping.IMPORT_ERROR.getResultTemplate();
        context.put("errorMessage", error);//NON-NLS

        SharedWorkflowImportPlan plan = getPlanOrNull(request);
        context.put("source", (plan == null) ? null : plan.getSource());

        clearSessionAttributes(request.getSession());

        renderer.render(errorTemplate, context, response.getWriter());
    }

    private void showChooseZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ServletMapping servletMapping) throws IOException, ValidationException
    {
        addDataNotFoundInSessionParam(request, context);

        Long schemeId = getLongParameter(request, SCHEME_ID_PARAM);
        if (schemeId == null)
        {
            schemeId = getSessionVar(request, SessionVar.SCHEME_ID);
        }

        Long projectId = getLongParameter(request, PROJECT_ID_PARAM);
        if (projectId == null)
        {
            projectId = getSessionVar(request, SessionVar.PROJECT_ID);
        }

        context.put("cancelUrl", getCancelUrl(projectId, schemeId, request));
        context.put("projectId", (projectId == null) ? "" : projectId);
        context.put("schemeId", (schemeId == null) ? "" : schemeId);

        setNavigationPaths(context, servletMapping);
        context.put("isSysAdmin", isSystemAdmin());
        renderer.render(servletMapping.getResultTemplate(), context, response.getWriter());
    }

    private void addDataNotFoundInSessionParam(HttpServletRequest request, Map<String, Object> context)
    {
        String dataNotFoundInSession = request.getParameter(DATA_NOT_FOUND_IN_SESSION_PARAM);
        if (StringUtils.isNotEmpty(dataNotFoundInSession))
        {
            context.put("errorMessage", i18n.getText("wfshare.exception.import.data.not.found.in.session"));
        }
    }

    private String getCancelUrl(HttpServletRequest request)
    {
        Long schemeId = getSessionVar(request, SessionVar.SCHEME_ID);
        Long projectId = getSessionVar(request, SessionVar.PROJECT_ID);
        return getCancelUrl(projectId, schemeId, request);
    }

    private String getCancelUrl(Long projectId, Long schemeId, HttpServletRequest request)
    {
        // showSuccess() method appends parameter to the cancelUrl assuming that there are no existing parameters.

        String cancelUrl = "/secure/admin/workflows/ListWorkflows.jspa";
        if (projectId != null)
        {
            Project project = projectManager.getProjectObj(projectId);
            if (project != null)
            {
                cancelUrl = "/plugins/servlet/project-config/" + project.getKey() + "/workflows";
            }
        }
        else if (schemeId != null)
        {
            cancelUrl = "/secure/admin/EditWorkflowScheme.jspa?schemeId=" + schemeId;
        }

        return request.getContextPath() + cancelUrl;
    }

    private void doDownloadMarketplaceZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException
    {
        try
        {
            clearSessionAttributes(request.getSession());

            String downloadUrl = request.getParameter(BUNDLE_DOWNLOAD_URL_FIELD_NAME);
            String filename = request.getParameter(BUNDLE_FILENAME_FIELD_NAME);

            request.getSession().setAttribute(SessionVar.WFSHARE_WORKFLOW_NAME.name(), createWorkflowNameFromFilename(filename));

            SharedWorkflowImportPlan plan = createPlan(pacFileDownloader.downloadBundle(downloadUrl));

            request.getSession().setAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name(), plan);

            Long schemeId = getLongParameter(request, SCHEME_ID_PARAM);
            addToSession(request, SessionVar.SCHEME_ID, schemeId);

            Long projectId = getLongParameter(request, PROJECT_ID_PARAM);
            addToSession(request, SessionVar.PROJECT_ID, projectId);

            context.put("cancelUrl", getCancelUrl(projectId, schemeId, request));
        }
        catch (ValidationException e)
        {
            sendError(response, e, false);
        }
        catch (Exception e)
        {
            sendError(response, e, true);
        }
    }

    private void sendError(HttpServletResponse response, Exception exception, boolean technical) throws IOException
    {
        LOG.debug(exception.getMessage(), exception);

        response.setContentType("application/json;charset=utf-8");
        JSONObject error = new JSONObject();
        try
        {
            error.put("errorMessage", exception.getMessage());
            error.put("technical", technical);
        }
        catch (JSONException jsonException)
        {
            throw new RuntimeException(jsonException);
        }
        response.getWriter().write(error.toString());
    }

    private void doUploadZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException, ValidationException
    {
        if (!isSystemAdmin())
        {
            throw new ValidationException(i18n.getText("wfshare.error.not.sys.admin"));
        }

        clearSessionAttributes(request.getSession());

        SharedWorkflowImportPlan plan = null;

        if (ServletFileUpload.isMultipartContent(request))
        {
            try
            {
                ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory((int) ImporterConstants.MAX_STREAM, null));
                upload.setSizeMax(ImporterConstants.MAX_STREAM);
                @SuppressWarnings("unchecked")
                List<FileItem> fileItems = upload.parseRequest(request);
                Long schemeId = null, projectId = null;

                for (FileItem item : fileItems)
                {
                    String fieldName = item.getFieldName();

                    if (!item.isFormField())
                    {
                        String fileName = item.getName();

                        request.getSession().setAttribute(SessionVar.WFSHARE_WORKFLOW_NAME.name(), createWorkflowNameFromFilename(fileName));

                        InputStream is = item.getInputStream();
                        try
                        {
                            plan = createPlan(factory.bundle(is, WorkflowBundle.BundleSource.MANUAL));
                        }
                        finally
                        {
                            IOUtils.closeQuietly(is);
                        }
                    }
                    else
                    {
                        if (PROJECT_ID_PARAM.equals(fieldName))
                        {
                            projectId = parseLong(item.getString());
                            addToSession(request, SessionVar.PROJECT_ID, projectId);
                        }
                        else if (SCHEME_ID_PARAM.equals(fieldName))
                        {
                            schemeId = parseLong(item.getString());
                            addToSession(request, SessionVar.SCHEME_ID, schemeId);
                        }
                        else
                        {
                            throw new ValidationException(i18n.getText("wfshare.exception.uploading.by.field.not.supported"));
                        }
                    }
                }

                context.put("cancelUrl", getCancelUrl(projectId, schemeId, request));
            }
            catch (FileUploadBase.SizeLimitExceededException e)
            {
                throw new IOException(i18n.getText("wfshare.exception.workflow.too.big"), e);
            }
            catch (FileUploadException e)
            {
                throw new IOException(e.getMessage(), e);
            }
        }
        else
        {
            throw new ValidationException(i18n.getText("wfshare.exception.not.a.multipart.form.request"));
        }

        request.getSession().setAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name(), plan);

        response.sendRedirect(ServletMapping.IMPORT_SET_NAME.getPath());
    }

    private Long getLongParameter(HttpServletRequest request, String name) throws ValidationException
    {
        return parseLong(request.getParameter(name));
    }

    private Long parseLong(String longString) throws ValidationException
    {
        if (StringUtils.isEmpty(longString))
        {
            return null;
        }

        if (!StringUtils.isNumeric(longString))
        {
            throw new ValidationException(i18n.getText("wfshare.exception.import.invalid.id"));
        }

        return Long.parseLong(longString);
    }

    private void addToSession(HttpServletRequest request, SessionVar sessionVar, Object value)
    {
        request.getSession().setAttribute(sessionVar.name(), value);
    }

    private SharedWorkflowImportPlan createPlan(WorkflowBundle bundle)
    {
        return new SharedWorkflowImportPlan(pluginAccessor, bundle);
    }

    private void showChooseName(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws DataNotFoundInSessionException, IOException
    {
        SharedWorkflowImportPlan plan = getPlan(request);
        String workflowName = getSessionAttributeOrNull(request, SessionVar.WFSHARE_WORKFLOW_NAME.name());

        showChooseName(response, context, plan, workflowName);
    }

    private void showChooseName(HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan, String workflowName) throws IOException
    {
        plan.setWorkflowName(workflowName);

        context.put("suggestedWorkflowName", workflowName);//NON-NLS

        setNavigationPaths(context, ServletMapping.IMPORT_SET_NAME);
        renderer.render(ServletMapping.IMPORT_SET_NAME.getResultTemplate(), context, response.getWriter());
    }

    public void doSetName(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws DataNotFoundInSessionException, IOException
    {
        SharedWorkflowImportPlan plan = getPlan(request);

        String sessionName = getSessionAttributeOrNull(request, SessionVar.WFSHARE_WORKFLOW_NAME.name());
        String chosenName = request.getParameter(WF_NAME_FIELD_NAME);

        if (null == chosenName && null != sessionName)
        {
            chosenName = sessionName;
        }

        ErrorCollection errorCollection = new SimpleErrorCollection();
        if (WorkflowUtil.isAcceptableName(chosenName, WF_NAME_FIELD_NAME, errorCollection))
        {
            if (workflowManager.getWorkflow(chosenName) != null)
            {
                errorCollection.addError(WF_NAME_FIELD_NAME, i18n.getText("wfshare.import.screen.name.error.exists"));
            }
        }

        if (errorCollection.getErrors().containsKey(WF_NAME_FIELD_NAME))
        {
            context.put("errorMessage", errorCollection.getErrors().get(WF_NAME_FIELD_NAME));

            showChooseName(response, context, plan, chosenName);
        }
        else
        {
            plan.setWorkflowName(chosenName);

            request.getSession().setAttribute(SessionVar.WFSHARE_WORKFLOW_NAME.name(), chosenName);

            response.sendRedirect(ServletMapping.IMPORT_MAP_STATUSES.getPath());
        }
    }

    private void showMapStatuses(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws DataNotFoundInSessionException, IOException
    {
        SharedWorkflowImportPlan plan = getPlan(request);

        Map<String, StatusMapping> statusHolders = getSessionAttributeOrNull(request, SessionVar.WFSHARE_STATUS_HOLDERS.name());
        if (null == statusHolders)
        {
            statusHolders = workflowStatusHelper.getStatusHolders(plan.getWorkflowXml());
            request.getSession().setAttribute(SessionVar.WFSHARE_STATUS_HOLDERS.name(), statusHolders);
        }

        context.put("jiraStatuses", getJiraStatuses());//NON-NLS
        context.put("statusHolders", Ordering.from(StatusMapping.OLD_NAME_ORDER).sortedCopy(statusHolders.values()));//NON-NLS

        addXsrfToken(context, request);

        setNavigationPaths(context, ServletMapping.IMPORT_MAP_STATUSES);

        renderer.render(ServletMapping.IMPORT_MAP_STATUSES.getResultTemplate(), context, response.getWriter());
    }

    private void doMapStatuses(HttpServletRequest request, HttpServletResponse response) throws DataNotFoundInSessionException, ValidationException, IOException
    {
        SharedWorkflowImportPlan plan = getPlan(request);
        Map<String, StatusMapping> statusHolders = getSessionAttribute(request, SessionVar.WFSHARE_STATUS_HOLDERS.name(), i18n.getText("wfshare.exception.status.holders.not.found.in.session"));

        Map<String, String[]> statusMap = getStatusParams(request);

        List<StatusMapping> statusMappings;
        if (null != statusMap && !statusMap.isEmpty())
        {
            statusMappings = createStatusMappings(statusHolders, statusMap);
            request.getSession().setAttribute(SessionVar.WFSHARE_STATUS_MAPPING.name(), statusMappings);
        }
        else
        {
            statusMappings = getSessionAttributeOrNull(request, SessionVar.WFSHARE_STATUS_MAPPING.name());
        }

        plan.setStatusMappings(statusMappings);

        response.sendRedirect(ServletMapping.IMPORT_VIEW_SUMMARY.getPath() + "?atl_token=" + request.getParameter("atl_token"));
    }

    private void showSummary(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context)
            throws IOException, DataNotFoundInSessionException, ServletException
    {
        SharedWorkflowImportPlan plan = getPlan(request);

        List<String> newStatuses = Ordering.natural().immutableSortedCopy(transform(filter(plan.getStatusMappings(),
                new Predicate<StatusMapping>()
                {
                    @Override
                    public boolean apply(StatusMapping statusMapping)
                    {
                        return statusMapping.isNewStatus();
                    }
                }),
                new Function<StatusMapping, String>()
                {
                    @Override
                    public String apply(StatusMapping statusMapping)
                    {
                        return statusMapping.getNewName();
                    }
                }));

        List<String> plugins = Ordering.from(String.CASE_INSENSITIVE_ORDER)
                .immutableSortedCopy(transform(plan.getPluginInfo(), WorkflowExtensionsPluginInfo.GET_NAME));

        List<ScreenInfo> screens = Ordering.from(ScreenInfo.ORDER_NAME)
                .immutableSortedCopy(transform(plan.getScreenInfo(), new Function<ScreenInfo, ScreenInfo>()
                {
                    @Override
                    public ScreenInfo apply(ScreenInfo screenInfo)
                    {
                        return new ScreenInfo(screenInfo.getOriginalId(), screenCreator.getValidName(screenInfo.getName()), screenInfo.getDescription(), screenInfo.getTabs());
                    }
                }));

        List<CustomFieldBean> enabledFields = getFields(plan.getAllowedEnabledCustomFields(pluginAccessor));

        List<CustomFieldBean> disabledFields = getFields(plan.getAllowedDisabledCustomFields(pluginAccessor));
        Collection<String> disabledFieldTypes = Lists.transform(disabledFields, new Function<CustomFieldBean, String>()
        {
            @Override
            public String apply(final CustomFieldBean disabledField)
            {
                return disabledField.getType();
            }
        });

        boolean nothingToCreate = newStatuses.isEmpty() && screens.isEmpty() && enabledFields.isEmpty();

        if (nothingToCreate)
        {
            doImport(request, response);
        }
        else
        {
            context.put("workflowName", plan.getWorkflowName());
            context.put("enabledFields", enabledFields);
            context.put("disabledFieldTypes", disabledFieldTypes);
            context.put("newStatuses", newStatuses);
            context.put("screens", screens);
            context.put("plugins", plugins);
            context.put("isSysAdmin", isSystemAdmin());

            addXsrfToken(context, request);

            setNavigationPaths(context, ServletMapping.IMPORT_WORKFLOW, ServletMapping.IMPORT_MAP_STATUSES);

            renderer.render(ServletMapping.IMPORT_VIEW_SUMMARY.getResultTemplate(), context, response.getWriter());
        }
    }

    private List<CustomFieldBean> getFields(List<CustomFieldInfo> info)
    {
        return Ordering.natural().immutableSortedCopy(transform(info, new Function<CustomFieldInfo, CustomFieldBean>()
        {
            @Override
            public CustomFieldBean apply(CustomFieldInfo input)
            {
                CustomFieldType customFieldType = getCustomFieldType(input.getTypeModuleKey());
                String type = (customFieldType == null) ? null : customFieldType.getName();
                String description = (customFieldType == null) ? null : customFieldType.getDescription();

                return new CustomFieldBean(input.getName(), type, description);
            }
        }));
    }

    private CustomFieldType getCustomFieldType(String key)
    {
        ModuleDescriptor module = pluginAccessor.getPluginModule(key);

        if (module instanceof CustomFieldTypeModuleDescriptor)
        {
            return (CustomFieldType) module.getModule();
        }

        return null;
    }

    private void doImport(HttpServletRequest request, HttpServletResponse response) throws IOException, DataNotFoundInSessionException, ServletException
    {
        if (checkXsrf(request, response))
        {
            return;
        }
        if (enforceAdminLoginForImport(request, response))
        {
            return;
        }

        JiraWorkflowSharingImporter importer = importerFactory.newImporter();

        SharedWorkflowImportPlan plan = getPlan(request);
        importer.importWorkflow(plan);

        response.sendRedirect(ServletMapping.IMPORT_WORKFLOW_SUCCESS.getPath());
    }

    private void showSuccess(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException, DataNotFoundInSessionException
    {
        SharedWorkflowImportPlan plan = getPlan(request);

        context.put("source", plan.getSource());

        String notes = plan.getNotes();
        if (notes != null)
        {
            context.put("notes", notes);

            int rowsInNotes = notes.split("\r\n|\r|\n").length;
            int rowsInNotesContainer = Math.max(rowsInNotes, 5);
            context.put("rowsInNotesContainer", rowsInNotesContainer);
        }

        if (getSessionVar(request, SessionVar.PROJECT_ID) != null || getSessionVar(request, SessionVar.SCHEME_ID) != null)
        {
            String cancelUrl = (String) context.get("cancelUrl");
            cancelUrl += "#workflowName=" + encodeUrl(plan.getWorkflowName());
            context.put("cancelUrl", cancelUrl);
        }

        clearSessionAttributes(request.getSession());

        renderer.render(ServletMapping.IMPORT_WORKFLOW_SUCCESS.getResultTemplate(), context, response.getWriter());
    }

    private Long getSessionVar(HttpServletRequest request, SessionVar sessionVar)
    {
        return getSessionAttributeOrNull(request, sessionVar.name());
    }

    private boolean enforceAdminLoginForImport(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        try
        {
            webSudoManager.willExecuteWebSudoRequest(request);
        }
        catch (WebSudoSessionException wse)
        {
            response.sendRedirect(request.getContextPath() + "/secure/admin/WebSudoAuthenticate!default.jspa?webSudoDestination="
                    + encodeUrl(request.getServletPath() + request.getPathInfo() + "?atl_token=" + getXsrfToken(request)));

            return true;
        }

        return false;
    }

    private SharedWorkflowImportPlan getPlan(HttpServletRequest request) throws DataNotFoundInSessionException
    {
        SharedWorkflowImportPlan plan = getPlanOrNull(request);

        if (plan != null)
        {
            return plan;
        }

        throw new DataNotFoundInSessionException();
    }

    private SharedWorkflowImportPlan getPlanOrNull(HttpServletRequest request)
    {
        return getSessionAttributeOrNull(request, SessionVar.WFSHARE_IMPORT_PLAN.name());
    }

    private List<StatusMapping> createStatusMappings(Map<String, StatusMapping> statusHolders, Map<String, String[]> statusParams) throws ValidationException
    {
        List<StatusMapping> mappings = new ArrayList<StatusMapping>();

        if (!statusParams.isEmpty())
        {
            for (Map.Entry<String, String[]> entry : statusParams.entrySet())
            {
                String oldId = StringUtils.substringAfterLast(entry.getKey(), "-");

                if (!statusHolders.containsKey(oldId))
                {
                    throw new ValidationException(i18n.getText("wfshare.exception.unknown.status.id", oldId));
                }

                //if we're creating a new status, use the new name, else use the original name for summary display
                String newId = entry.getValue()[0];

                StatusMapping oldMappingEntry = statusHolders.get(oldId);
                String originalName = oldMappingEntry.getOriginalName();

                if (!newId.equals(oldMappingEntry.getNewId()))
                {
                    oldMappingEntry.setNewId(newId);
                }

                String newName = newId.equals("-1")
                                 ? oldMappingEntry.getNewName()
                                 : workflowStatusHelper.getNameForStatusId(newId);

                mappings.add(new StatusMapping(oldId, originalName, newId, newName));
            }
        }

        return mappings;
    }

    private Map<String, String[]> getStatusParams(HttpServletRequest request)
    {
        @SuppressWarnings ("unchecked") final Map<String, String[]> parameterMap = request.getParameterMap();

        return ImmutableMap.copyOf(Maps
                .filterKeys(parameterMap, new Predicate<String>()
                {
                    @Override
                    public boolean apply(String key)
                    {
                        return key.startsWith(PARAM_STATUS_FOR_PREFIX);
                    }
                }));
    }

    private String createWorkflowNameFromFilename(String fileName)
    {
        String workflowName = WordUtils.capitalizeFully(FilenameUtils.removeExtension(fileName).replaceAll("[_:\\\\/*?|<>-]", " "));
        String suggestedName = workflowName;

        if (workflowManager.workflowExists(workflowName))
        {
            int x = 2;
            while (workflowManager.workflowExists(suggestedName))
            {
                suggestedName = i18n.getText("wfshare.import.prefix", String.valueOf(x), workflowName);
                x++;
            }
        }

        return suggestedName;
    }

    public List<Status> getJiraStatuses()
    {
        return workflowStatusHelper.getJiraStatuses();
    }

    private void setNavigationPaths(Map<String, Object> context, ServletMapping servletMapping)
    {
        setNavigationPaths(context, servletMapping, servletMapping.previous());
    }

    private void setNavigationPaths(Map<String, Object> context, ServletMapping next, ServletMapping previous)
    {
        String nextPath = "/plugins/servlet/wfshare-import/" + next.getPath();

        String backPath = "";
        if (null != previous)
        {
            backPath = "/plugins/servlet/wfshare-import/" + previous.getPath();
        }

        context.put("nextUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + nextPath);
        context.put("backUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + backPath);
    }

    @Override
    protected void clearSessionAttributes(HttpSession session)
    {
        super.clearSessionAttributes(session);
        for (SessionVar sessionVar : SessionVar.values())
        {
            session.removeAttribute(sessionVar.name());
        }
    }

    public static class CustomFieldBean implements Comparable<CustomFieldBean>
    {
        private final String name;
        private final String type;
        private final String description;

        public CustomFieldBean(String name, String type, String description)
        {
            this.name = name;
            this.type = type;
            this.description = description;
        }

        public String getName()
        {
            return name;
        }

        public String getType()
        {
            return type;
        }

        public String getDescription()
        {
            return description;
        }

        @Override
        public int compareTo(CustomFieldBean o)
        {
            return name.compareToIgnoreCase(o.name);
        }
    }
}
