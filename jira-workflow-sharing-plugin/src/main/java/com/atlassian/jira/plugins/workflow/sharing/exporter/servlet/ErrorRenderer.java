package com.atlassian.jira.plugins.workflow.sharing.exporter.servlet;

import com.atlassian.jira.plugins.workflow.sharing.servlet.ServletMapping;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

public class ErrorRenderer
{
    private final TemplateRenderer renderer;

    public ErrorRenderer(TemplateRenderer renderer)
    {
        this.renderer = renderer;
    }

    public void render(String error, HttpServletResponse response, Map<String, Object> context) throws IOException
    {
        String errorTemplate = ServletMapping.EXPORT_ERROR.getResultTemplate();
        context.put("errorMessage", error);//NON-NLS

        renderer.render(errorTemplate, context, response.getWriter());
    }
}
