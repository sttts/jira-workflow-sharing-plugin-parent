package com.atlassian.jira.plugins.workflow.sharing.exporter;

import com.atlassian.jira.workflow.JiraWorkflow;

public interface WorkflowExportNotesProvider
{
    String createNotes(JiraWorkflow workflow);
}
