package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;

import java.util.Map;

public interface ScreenCreator
{
    void removeScreen(Long screenId);

    FieldScreen createScreen(ScreenInfo screenInfo);

    void addScreenTabs(FieldScreen screen, ScreenInfo screenInfo, Map<String, String> createdFieldsMapping);

    String getValidName(String originalName);
}
