package com.atlassian.jira.plugins.workflow.sharing;

public class WorkflowLayoutKeyFinderImpl implements WorkflowLayoutKeyFinder
{
    @Override
    public String getActiveLayoutKey(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                    setWorkflowName(workflowName).
                    setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.LIVE).
                    build();
    }

    @Override
    public String getDraftLayoutKey(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                setWorkflowName(workflowName).
                setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.DRAFT).
                build();
    }
}
