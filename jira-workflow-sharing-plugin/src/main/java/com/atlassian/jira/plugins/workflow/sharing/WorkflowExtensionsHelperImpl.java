package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.jira.workflow.condition.AllowOnlyAssignee;
import com.atlassian.jira.workflow.condition.AllowOnlyReporter;
import com.atlassian.jira.workflow.condition.PermissionCondition;
import com.atlassian.jira.workflow.function.event.FireIssueEventFunction;
import com.atlassian.jira.workflow.function.issue.AssignToCurrentUserFunction;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class WorkflowExtensionsHelperImpl implements WorkflowExtensionsHelper
{
    private static final String[] CLASSES_WITH_USERNAME_ARGS = new String[] { AllowOnlyAssignee.class.getName(), AllowOnlyReporter.class.getName(),
                                                                              PermissionCondition.class.getName(), AssignToCurrentUserFunction.class.getName() };
    
    private final ModuleDescriptorLocator moduleDescriptorLocator;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowService workflowService;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final WorkflowManager workflowManager;
    private final WhitelistChecker whitelistChecker;

    public WorkflowExtensionsHelperImpl(ModuleDescriptorLocator moduleDescriptorLocator, JiraAuthenticationContext jiraAuthenticationContext,
                                        WorkflowService workflowService, WorkflowScreensHelper workflowScreensHelper,
                                        WorkflowManager workflowManager, WhitelistChecker whitelistChecker) {

        this.moduleDescriptorLocator = moduleDescriptorLocator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowService = workflowService;
        this.workflowScreensHelper = workflowScreensHelper;
        this.workflowManager = workflowManager;
        this.whitelistChecker = whitelistChecker;
    }

    @Override
    public JiraWorkflow getWorkflowForNameAndMode(String name, String mode)
    {
        JiraWorkflow workflow;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());

        if ("draft".equalsIgnoreCase(mode))//NON-NLS
        {
            workflow = workflowService.getDraftWorkflow(jiraServiceContext, name);
        }
        else
        {
            workflow = workflowService.getWorkflow(jiraServiceContext, name);
        }

        return workflow;
    }

    @Override
    public void removeIllegalComponents(JiraWorkflow jiraWorkflow)
    {
        doRemove(jiraWorkflow, null, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public JiraWorkflow copyAndRemoveIllegalComponents(JiraWorkflow workflow)
    {
        workflow = copy(workflow);

        removeIllegalComponents(workflow);

        return workflow;
    }

    private JiraWorkflow copy(JiraWorkflow workflow)
    {
        try
        {
            WorkflowDescriptor descriptorCopy = WorkflowUtil.convertXMLtoWorkflowDescriptor(WorkflowUtil.convertDescriptorToXML(workflow.getDescriptor()));
            return new ConfigurableJiraWorkflow(workflow.getName(), descriptorCopy, workflowManager);
        }
        catch (FactoryException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public RemovedItems getRemovedItems(JiraWorkflow workflow, boolean includeCustomFields)
    {
        RemovedItems removedItems = new RemovedItems();

        doRemove(workflow, removedItems, includeCustomFields);

        return removedItems;
    }

    @SuppressWarnings("unchecked")
    private void doRemove(JiraWorkflow workflow, RemovedItems removedItems, boolean includeCustomFields)
    {
        boolean delete = (removedItems == null);

        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            RemovedItems.ActionRemovedItems actionRemovedItems = new RemovedItems.ActionRemovedItems(action);
            if (removedItems != null)
            {
                removedItems.add(actionRemovedItems);
            }

            discardPostFunctions(action.getUnconditionalResult().getPostFunctions(), delete, actionRemovedItems);
            discardPostFunctions(action.getPostFunctions(), delete, actionRemovedItems);

            discardValidators(action.getUnconditionalResult().getValidators(), delete, actionRemovedItems);
            discardValidators(action.getValidators(), delete, actionRemovedItems);

            removeConditions(action, delete, actionRemovedItems);

            if (removedItems != null && includeCustomFields)
            {
                Set<String> customFields = workflowScreensHelper.getPluginRelatedClassnamesForScreenFromAction(action);
                for (String customField : customFields)
                {
                    actionRemovedItems.addCustomFieldClass(customField);
                }
            }
        }
    }

    private void discardPostFunctions(List<FunctionDescriptor> functions, boolean delete,
                                      RemovedItems.ActionRemovedItems actionRemovedItems)
    {
        for (Iterator<FunctionDescriptor> iterator = functions.iterator(); iterator.hasNext(); )
        {
            FunctionDescriptor function = iterator.next();

            @SuppressWarnings("unchecked")
            Set<Map.Entry<String, String>> args = (Set<Map.Entry<String, String>>) function.getArgs().entrySet();

            PostFunctionArgumentProcessor processor = new PostFunctionArgumentProcessor(delete);
            String className = getNotAllowedClassName(iterator, delete, args, processor);

            if (actionRemovedItems != null)
            {
                if (className != null)
                {
                    actionRemovedItems.addFunctionClass(className);
                }

                if (processor.isReplacedEventId())
                {
                    actionRemovedItems.markReplacedEventId();
                }
            }
        }
    }

    private void discardValidators(List<ValidatorDescriptor> validators, boolean delete,
                                   RemovedItems.ActionRemovedItems actionRemovedItems)
    {
        for (Iterator<ValidatorDescriptor> iterator = validators.iterator(); iterator.hasNext(); )
        {
            ValidatorDescriptor validator = iterator.next();

            @SuppressWarnings("unchecked")
            Set<Map.Entry<String, String>> args = (Set<Map.Entry<String, String>>) validator.getArgs().entrySet();

            String className = getNotAllowedClassName(iterator, delete, args);
            if (className != null && actionRemovedItems != null)
            {
                actionRemovedItems.addValidatorClass(className);
            }
        }
    }

    public void removeConditions(ActionDescriptor action, boolean delete,
                                  RemovedItems.ActionRemovedItems actionRemovedItems)
    {
        if (null != action.getRestriction())
        {
            ConditionsDescriptor rootConditions = action.getRestriction().getConditionsDescriptor();
            if (null != rootConditions)
            {
                int rootConditionNumber = discardConditions(rootConditions, delete, actionRemovedItems);
                if (rootConditionNumber == 0)
                {
                    action.setRestriction(null);
                }
                else if (rootConditionNumber == 1)
                {
                    Object singleChild = rootConditions.getConditions().get(0);

                    if (singleChild instanceof ConditionsDescriptor)
                    {
                        action.getRestriction().setConditionsDescriptor((ConditionsDescriptor) singleChild);
                    }
                }
            }
        }
    }

    private int discardConditions(ConditionsDescriptor rootConditions, boolean delete,
                                   RemovedItems.ActionRemovedItems actionRemovedItems)
    {
        if (null == rootConditions)
        {
            return 0;
        }

        @SuppressWarnings("unchecked")
        List<Object> nestedConditions = rootConditions.getConditions();

        for (ListIterator<Object> iterator = nestedConditions.listIterator(); iterator.hasNext(); )
        {
            Object o = iterator.next();

            if (o instanceof ConditionDescriptor)
            {
                ConditionDescriptor condition = (ConditionDescriptor) o;

                @SuppressWarnings("unchecked")
                Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) condition.getArgs().entrySet();

                String className = getNotAllowedClassName(iterator, delete, args);
                if (className != null && actionRemovedItems != null)
                {
                    actionRemovedItems.addConditionClass(className);
                }
            }
            else if (o instanceof ConditionsDescriptor)
            {
                ConditionsDescriptor conditionsDescriptor = (ConditionsDescriptor) o;

                discardOrMergeConditionGroup(conditionsDescriptor, iterator, delete, actionRemovedItems);
            }
        }

        return nestedConditions.size();
    }

    private void discardOrMergeConditionGroup(ConditionsDescriptor conditionsDescriptor, ListIterator<Object> iterator,
                                              boolean delete, RemovedItems.ActionRemovedItems actionRemovedItems)
    {
        int compositeSize = discardConditions(conditionsDescriptor, delete, actionRemovedItems);

        if (compositeSize == 0)
        {
            iterator.remove();
        }
        else if (compositeSize == 1)
        {
            Object singleChild = conditionsDescriptor.getConditions().get(0);

            iterator.set(singleChild);
        }
    }

    private String getNotAllowedClassName(Iterator<?> iterator, boolean delete, Set<Map.Entry<String, String>> args)
    {
        WhitelistCheckingArgumentProcessor processor = new WhitelistCheckingArgumentProcessor();

        return getNotAllowedClassName(iterator, delete, args, processor);
    }

    private String getNotAllowedClassName(Iterator<?> iterator, boolean delete, Set<Map.Entry<String, String>> args, WhitelistCheckingArgumentProcessor processor)
    {
        processor.process(args);
        String notAllowedClassName = processor.getNotAllowedClassName();

        if (notAllowedClassName != null && delete)
        {
            iterator.remove();
        }

        return notAllowedClassName;
    }

    private abstract class ArgumentProcessor
    {
        void process(Set<Map.Entry<String, String>> args)
        {
            Set<String> classNames = getClassnamesFromArgs(args);

            process(args, classNames);
        }

        abstract void process(Set<Map.Entry<String, String>> args, Set<String> classNames);
    }

    private class WhitelistCheckingArgumentProcessor extends ArgumentProcessor
    {
        private String notAllowedClassName;

        @Override
        void process(Set<Map.Entry<String, String>> args, Set<String> classNames)
        {
            for (String className : classNames)
            {
                if (!whitelistChecker.isAllowed(className))
                {
                    notAllowedClassName = className;

                    return;
                }

                removeUsernameArg(args, className);
            }
        }

        String getNotAllowedClassName()
        {
            return notAllowedClassName;
        }
    }

    private class PostFunctionArgumentProcessor extends WhitelistCheckingArgumentProcessor
    {
        private boolean replacedEventId;

        private final boolean replace;

        private PostFunctionArgumentProcessor(boolean replace)
        {
            this.replace = replace;
        }

        @Override
        void process(Set<Map.Entry<String, String>> args, Set<String> classNames)
        {
            super.process(args, classNames);

            if (getNotAllowedClassName() == null)
            {
                for (String className : classNames)
                {
                    if (FireIssueEventFunction.class.getName().equals(className))
                    {
                        replacedEventId = checkAndReplaceEventId(args, replace);
                    }
                }
            }
        }

        public boolean isReplacedEventId()
        {
            return replacedEventId;
        }
    }

    private void removeUsernameArg(Set<Map.Entry<String, String>> args, String className)
    {
        if (ArrayUtils.contains(CLASSES_WITH_USERNAME_ARGS, className))
        {
            for (Iterator<Map.Entry<String, String>> i = args.iterator(); i.hasNext();)
            {
                Map.Entry<String, String> entry = i.next();
                if ("username".equals(entry.getKey()))
                {
                    i.remove();

                    break;
                }
            }
        }
    }

    private boolean checkAndReplaceEventId(Set<Map.Entry<String, String>> args, boolean replace)
    {
        for (Map.Entry<String, String> entry : args)
        {
            if ("eventTypeId".equals(entry.getKey()))
            {
                long value = Long.parseLong(entry.getValue());

                if (value > EventType.ISSUE_GENERICEVENT_ID)
                {
                    if (replace)
                    {
                        entry.setValue(String.valueOf(EventType.ISSUE_GENERICEVENT_ID));
                    }

                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public Multimap<Plugin, String> getRequiredPlugins(JiraWorkflow workflow)
    {
        SetMultimap<Plugin, String> pluginExtensionMap = HashMultimap.create();

        Set<String> extensionClassnames = getExtensionClassnames(workflow);
        for (String classname : extensionClassnames)
        {
            Collection<ModuleDescriptor> moduleDescriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(classname);
            for (ModuleDescriptor moduleDescriptor : moduleDescriptors)
            {
                Plugin plugin = moduleDescriptor.getPlugin();

                //make sure we ignore old v1 SYSTEM plugins (may not have the system flag set)
                if (plugin.getKey().startsWith("com.atlassian.jira.plugin.system"))//NON-NLS
                {
                    continue;
                }

                pluginExtensionMap.put(plugin, classname);
            }
        }

        return pluginExtensionMap;
    }

    private Set<String> getExtensionClassnames(JiraWorkflow workflow)
    {
        Set<String> uniqueClassnames = new HashSet<String>();
        
        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            uniqueClassnames.addAll(workflowScreensHelper.getClassnamesForScreenFromAction(action));
            
            uniqueClassnames.addAll(getFunctionClassnames(action));
            uniqueClassnames.addAll(getValidatorClassnames(action));

            if(null != action.getRestriction())
            {
                uniqueClassnames.addAll(getConditionClassnames(action.getRestriction().getConditionsDescriptor()));
            }
        }
        
        return uniqueClassnames;
    }

    private Set<String> getFunctionClassnames(ActionDescriptor action)
    {
        List<FunctionDescriptor> functions = action.getUnconditionalResult().getPostFunctions();
        functions.addAll(action.getPostFunctions());
        
        Set<String> classNames = new HashSet<String>();
        for (FunctionDescriptor function : functions)
        {
            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) function.getArgs().entrySet();
            classNames.addAll(getClassnamesFromArgs(args));
        }

        return classNames;
    }

    private Set<String> getValidatorClassnames(ActionDescriptor action)
    {
        List<ValidatorDescriptor> validators = action.getUnconditionalResult().getValidators();
        validators.addAll(action.getValidators());

        Set<String> classNames = new HashSet<String>();
        for (ValidatorDescriptor validator : validators)
        {
            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) validator.getArgs().entrySet();
            classNames.addAll(getClassnamesFromArgs(args));
        }

        return classNames;
    }

    private Set<String> getConditionClassnames(ConditionsDescriptor rootConditions)
    {
        Set<String> classNames = new HashSet<String>();

        if(null == rootConditions)
        {
            return classNames;
        }

        final List nestedConditions = rootConditions.getConditions();
        for (final Iterator iterator = nestedConditions.iterator(); iterator.hasNext(); )
        {
            final Object o = iterator.next();

            if (o instanceof ConditionDescriptor)
            {
                Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) ((ConditionDescriptor) o).getArgs().entrySet();
                classNames.addAll(getClassnamesFromArgs(args));
                
            }
            else if (o instanceof ConditionsDescriptor)
            {
                classNames.addAll(getConditionClassnames((ConditionsDescriptor) o));
            }
        }

        return classNames;
    }

    private Set<String> getClassnamesFromArgs(Set<Map.Entry<String, String>> args)
    {
        Set<String> classNames = new HashSet<String>();
        
        for(Map.Entry<String,String> entry : args)
        {
            if(entry.getKey().equals("class.name") && StringUtils.isNotBlank(entry.getValue()))
            {
                classNames.add(entry.getValue());
            }
        }
        
        return classNames;
    }
}