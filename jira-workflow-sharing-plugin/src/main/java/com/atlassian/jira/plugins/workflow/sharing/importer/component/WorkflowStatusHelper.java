package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.importer.StatusMapping;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.List;
import java.util.Map;

public interface WorkflowStatusHelper
{
    Map<String,StatusMapping> getStatusHolders(String xml);

    List<Status> getJiraStatuses();

    void removeStatus(String id) throws GenericEntityException;

    Status createStatus(String newName, String newStatusDefaultIcon) throws GenericEntityException;
    
    String getNameForStatusId(String id);

    String getValidStatusName(String newName);
}
