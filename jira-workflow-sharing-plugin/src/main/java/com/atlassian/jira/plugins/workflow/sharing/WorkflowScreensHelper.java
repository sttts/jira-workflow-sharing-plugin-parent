package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ActionDescriptor;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public interface WorkflowScreensHelper
{
    public static final String FIELDSCREEN_VIEW = "fieldscreen";
    public static final String FIELDSCREEN_ID_NAME = "jira.fieldscreen.id";

    String getScreensJson(JiraWorkflow workflow) throws IOException;

    Set<String> getClassnamesForScreen(String screenId);

    Set<String> getClassnamesForScreenFromAction(ActionDescriptor action);

    Set<String> getPluginRelatedClassnamesForScreenFromAction(ActionDescriptor action);

    Iterable<String> getCustomFieldIdsForWorkflowScreens(JiraWorkflow workflow);

    void updateWorkflowScreenIds(ConfigurableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping);
    
}
