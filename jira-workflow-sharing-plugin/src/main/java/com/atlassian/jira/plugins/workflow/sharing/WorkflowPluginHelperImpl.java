package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;

public class WorkflowPluginHelperImpl implements WorkflowPluginHelper
{
    private final ModuleDescriptorLocator moduleDescriptorLocator;

    public WorkflowPluginHelperImpl(ModuleDescriptorLocator moduleDescriptorLocator)
    {
        this.moduleDescriptorLocator = moduleDescriptorLocator;
    }

    @Override
    public boolean isFromPlugin(String className)
    {
        if (StringUtils.isEmpty(className))
        {
            return false;
        }

        Collection<ModuleDescriptor> moduleDescriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(className);
        for (ModuleDescriptor moduleDescriptor : moduleDescriptors)
        {
            Plugin plugin = moduleDescriptor.getPlugin();

            //make sure we ignore old v1 SYSTEM plugins (may not have the system flag set)
            if (plugin.getKey().startsWith("com.atlassian.jira.plugin.system"))//NON-NLS
            {
                continue;
            }

            return true;
        }

        return false;
    }
}
