package com.atlassian.jira.plugins.workflow.sharing.importer;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowBundle;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.List;

public final class SharedWorkflowImportPlan implements Serializable
{
    private static final long serialVersionUID = -6834060780888044072L;

    private final WorkflowBundle bundle;
    private final List<CustomFieldInfo> goodCustomFields;

    private volatile List<StatusMapping> statusMappings;
    private volatile String workflowName;

    public SharedWorkflowImportPlan(PluginAccessor pluginAccessor, WorkflowBundle bundle)
    {
        List<CustomFieldInfo> allowed = Lists.newArrayList();

        for (CustomFieldInfo info : bundle.getCustomFieldInfo())
        {
            if (pluginAccessor.getPluginModule(info.getTypeModuleKey()) != null)
            {
                String fieldPluginKey = StringUtils.stripToEmpty(info.getPluginKey());
                if (fieldPluginKey.startsWith("com.atlassian.jira.plugin.system."))
                {
                    allowed.add(info);
                }
            }
        }

        this.goodCustomFields = ImmutableList.copyOf(allowed);
        this.bundle = bundle;
    }

    public List<StatusMapping> getStatusMappings()
    {
        return statusMappings;
    }

    public void setStatusMappings(List<StatusMapping> statusMappings)
    {
        this.statusMappings = statusMappings;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public void setWorkflowName(String workflowName)
    {
        this.workflowName = workflowName;
    }

    public List<CustomFieldInfo> getAllowedEnabledCustomFields(PluginAccessor pluginAccessor)
    {
        return getAllowedCustomFields(new EnabledModulePredicate(pluginAccessor));
    }

    public List<CustomFieldInfo> getAllowedDisabledCustomFields(PluginAccessor pluginAccessor)
    {
        return getAllowedCustomFields(Predicates.not(new EnabledModulePredicate(pluginAccessor)));
    }

    private List<CustomFieldInfo> getAllowedCustomFields(Predicate<CustomFieldInfo> predicate)
    {
        return ImmutableList.copyOf(Collections2.filter(goodCustomFields, predicate));
    }

    private static class EnabledModulePredicate implements Predicate<CustomFieldInfo>
    {
        private final PluginAccessor pluginAccessor;

        EnabledModulePredicate(PluginAccessor pluginAccessor)
        {
            this.pluginAccessor = pluginAccessor;
        }

        @Override
        public boolean apply(CustomFieldInfo customFieldInfo)
        {
            return pluginAccessor.isPluginModuleEnabled(customFieldInfo.getTypeModuleKey());
        }
    }

    public List<ScreenInfo> getScreenInfo()
    {
        return ImmutableList.copyOf(bundle.getScreenInfo());
    }

    public List<WorkflowExtensionsPluginInfo> getPluginInfo()
    {
        return ImmutableList.copyOf(bundle.getPluginInfo());
    }

    public String getNotes()
    {
        return bundle.getNotes();
    }

    public WorkflowBundle.BundleSource getSource()
    {
        return bundle.getSource();
    }

    public String getWorkflowXml()
    {
        return bundle.getWorkflowXml();
    }

    public String getLayout()
    {
        return bundle.getLayout();
    }

    public String getAnnotations()
    {
        return bundle.getAnnotations();
    }
}
