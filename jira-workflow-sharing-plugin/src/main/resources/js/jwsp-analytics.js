AJS.namespace("JIRA.WSP.Analytics");

JIRA.WSP.Analytics.sendEvent = function (name, parameters) {
    if (AJS.EventQueue) {
        AJS.EventQueue.push({
            name: "jwsp." + name,
            properties: parameters || {}
        });
    }
};
