(function ($) {

    var localWFShareBundles = function () {

        var that = this;

        // Handle a click on a plugin row
        this.loadPlugins = function (page, projectId, schemeId) {
            AJS.$.ajax({
                url: AJS.contextPath() + "/rest/wfshare/1.0/workflowbundles/summary/" + page,
                type: 'get',
                cache: false,
                timeout: 60000,
                success: function (pluginsJson) {

                    $('.upm-bundles-loading-container').hide();

                    var plugins = pluginsJson.plugins;

                    for (var i = 0, len = plugins.length; i < len; i++) {
                        var plugin = plugins[i];
                        plugin.nextFormUrl = $("#wfshareNextFormUrl").val();
                        var pluginEntry = $(JIRA.Templates.WFShare.pluginEntry({
                            plugin: plugin,
                            projectId: projectId,
                            schemeId: schemeId
                        }));
                        $("#workflowBundleListContainer").append(pluginEntry);

                        if (wfshareBundles.importing === true) {
                            pluginEntry.find(".bundle-import-button").prop('disabled', true);
                        }
                    }

                    var $seeMore = $('.upm-plugins-see-more');
                    if (pluginsJson.nextOffset > -1) {
                        $seeMore.removeClass("hidden");
                        $seeMore.unbind('click').click(function (e) {
                            e.preventDefault();

                            if (!$seeMore.hasClass("disabled")) {
                                $seeMore.addClass("hidden");
                                $('.upm-bundles-loading-container').show();
                                wfshareBundles.loadPlugins(pluginsJson.nextOffset, projectId, schemeId);
                            }
                        });

                        if (wfshareBundles.importing === true) {
                            $seeMore.prop("disabled", true).addClass("disabled");
                        }
                    } else {
                        $seeMore.addClass("hidden");
                    }
                    
                },
                error: function () {
                    $('.upm-bundles-loading-container').hide();

                    that.showCouldNotContactMpacWarning("#upm-message-bar", false);
                }
            });
            
        };

        this.showCouldNotContactMpacWarning = function (target, closeable) {
            AJS.messages.warning(target, {
                body: AJS.I18n.getText("wfshare.import.screen.bundle.loading.error", '<a href="https://marketplace.atlassian.com/">', '</a>'),
                closeable: closeable
            });
        };

        this.pluginRowClick = function (e) {
            var target = $(e.target);
            if (!target.closest('.upm-plugin-actions').length) {
                wfshareBundles.togglePluginDetails(target.closest('div.upm-plugin'));
            }
        };

        this.togglePluginDetails = function (container) {
            if (container.hasClass('expanded')) {
                container.removeClass('expanded');
            } else {
                var details = container.find('div.upm-details');
                if (!details.hasClass('loaded') && !details.hasClass('loading')) {
                    container.addClass('loading');

                    AJS.$.ajax({
                        url: AJS.contextPath() + "/rest/wfshare/1.0/workflowbundles/details/" + container.attr('plugin-key'),
                        type: 'get',
                        cache: false,
                        timeout: 60000,
                        success:function (detailsJson) {
                            detailsJson.nextFormUrl = $("#wfshareNextFormUrl").val();
                            
                            details.append(JIRA.Templates.WFShare.pluginDetails({details:detailsJson}));
                            container.removeClass('loading').addClass('expanded');
                            details.addClass('loaded');
                            
                            var screenshotsContainer = $('div.upm-plugin-screenshots', details),
                                numScreenshots = detailsJson.media.screenshots.length || 0;
                            if (numScreenshots) {
                                $('a', screenshotsContainer).click(function (e) {
                                    e.preventDefault();
                                    $.fancybox(
                                        wfshareBundles.getScreenshotArray(detailsJson.media.screenshots),
                                        {
                                            changeSpeed: 0,
                                            cyclic: true,
                                            overlayColor: '#000',
                                            overlayOpacity: 0.8,
                                            speedIn: 0,
                                            speedOut: 0,
                                            transitionIn: 'none',
                                            transitionOut: 'none'
                                        });
                                });
                            }
                            
                        },
                        error: function () {
                            container.removeClass('loading');

                            var messageBar = container.find(".upm-plugin-message-bar");

                            that.showCouldNotContactMpacWarning(messageBar, true);
                        }
                    });
                } else {
                    container.addClass('expanded').trigger('pluginLoaded');
                }
            }
        };
        
        this.getScreenshotArray = function (screenshots) {
            var ss = [],
                s;
            for (var i = 0; i < screenshots.length; i++) {
                s = { 'href' : wfshareBundles.getScreenshotBinary(screenshots[i]) };
                screenshots[i].name && (s.title = screenshots[i].name);
                ss.push(s);
            }

            return ss;
        };

        this.getScreenshotBinary = function (screenshot) {
            for (var i = 0; i < screenshot.carouselImage.links.length; i++) {
                if (screenshot.carouselImage.links[i].rel === 'binary')
                {
                    return screenshot.carouselImage.links[i].href;
                }
            }

            return "not-found";
        };

        this.toggleMode = function (selectedId) {
            var uploadContainer = $("#upload-container");
            var upmContainer = $("#upm-container");
            var nextButton = $("#nextButton");

            var toHide;
            var toShow;
            if ("upm" === selectedId) {
                toHide = uploadContainer;
                toShow = upmContainer;
            } else if ("upload" === selectedId) {
                toHide = upmContainer;
                toShow = uploadContainer;
            }

            toHide.addClass("invisible");
            toShow.removeClass("invisible");
        };

        this.downloadBundle = function (e) {
            e.preventDefault();

            wfshareBundles.importing = true;

            var importForm = $(this).closest("form");

            $(".bundle-import-button").prop('disabled', true);
            $(".upm-plugins-see-more").prop('disabled', true).addClass("disabled");

            var indicator = importForm.parents(".upm-plugin-actions").find(".upm-bundles-downloading-indicator");
            indicator.show();

            var onRequestError = function (error, technical) {
                $(".bundle-import-button").prop('disabled', false);
                $(".upm-plugins-see-more").prop('disabled', false).removeClass("disabled");
                indicator.hide();

                var messageBar = importForm.parents(".upm-plugin-row").find(".upm-plugin-message-bar");

                if (technical === true) {
                    that.showCouldNotContactMpacWarning(messageBar, true);
                } else {
                    AJS.messages.error(messageBar, {
                        body: error,
                        closeable: true
                    });
                }

                wfshareBundles.importing = false;
            };

            AJS.$.ajax({
                url: importForm.attr("action"),
                data: importForm.serialize(),
                type: 'post',
                cache: false,
                timeout: 60000,
                success: function (response) {
                    if (response && response.errorMessage) {
                        onRequestError(response.errorMessage, response.technical);
                    } else {
                        window.location.href = AJS.contextPath() + "/plugins/servlet/wfshare-import/set-workflow-name";
                    }
                },
                error: function (jqXHR, textStatus) {
                    onRequestError(textStatus, true);
                }
            });
        };
    };

    window.wfshareBundles = new localWFShareBundles();

})(AJS.$);

AJS.$(function ($) {
    var plugins = $('div.upm-plugin-list.expandable div.upm-plugin');
    plugins.find('div.upm-plugin-row').live('click', wfshareBundles.pluginRowClick);
    plugins.find('.bundle-import-button').live('click', wfshareBundles.downloadBundle);

    $(".wfshare-bundle-toggle").click(function () {
        wfshareBundles.toggleMode(this.id);
    });

    var projectId = $("#projectId").val();
    var schemeId = $("#schemeId").val();

    if ($('.upm-plugin-list-container').length) {
        $('.upm-bundles-loading-container').show();
        wfshareBundles.loadPlugins(0, projectId, schemeId);
    }

    $("#wfShareImportFile").change(function () {
        $("#nextButton").prop("disabled", !$(this).val());
    }).change();

    // Using browser back button after uploading a bundle leaves the upload radiobutton checked.
    // In this case we check the Marketplace option again.
    $("#upm").prop("checked", true);
});
