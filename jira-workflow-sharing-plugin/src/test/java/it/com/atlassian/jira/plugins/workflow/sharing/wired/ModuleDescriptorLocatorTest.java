package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import java.io.File;
import java.util.Collection;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowModuleDescriptor;
import com.atlassian.jira.plugin.workflow.UpdateIssueFieldFunctionPluginFactory;
import com.atlassian.jira.plugins.workflow.sharing.ModuleDescriptorLocator;
import com.atlassian.jira.plugins.workflow.sharing.exporter.component.JiraWorkflowSharingExporterImpl;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.PluginTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.osgi.factory.descriptor.ComponentModuleDescriptor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import org.apache.commons.lang.StringUtils;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AtlassianPluginsTestRunner.class)
public class ModuleDescriptorLocatorTest
{

    private final ModuleDescriptorLocator moduleDescriptorLocator;
    private final PluginAccessor pluginAccessor;
    private final PluginController pluginController;
    private final PluginTestUtils pluginTestUtils;

    public ModuleDescriptorLocatorTest(ModuleDescriptorLocator moduleDescriptorLocator, PluginAccessor pluginAccessor, PluginController pluginController, PluginTestUtils pluginTestUtils)
    {
        this.moduleDescriptorLocator = moduleDescriptorLocator;
        this.pluginAccessor = pluginAccessor;
        this.pluginController = pluginController;
        this.pluginTestUtils = pluginTestUtils;
    }

    @AfterClass
    public void cleanup()
    {
        pluginTestUtils.clean();    
    }
    
    @Test
    public void nonWorkflowDescriptorFound() throws Exception
    {
        Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(JiraWorkflowSharingExporterImpl.class.getName());
        
        assertTrue("non workflow descriptor not found", !descriptors.isEmpty());
        
        ModuleDescriptor descriptor = descriptors.iterator().next();
        
        assertEquals(ComponentModuleDescriptor.class.getSimpleName(),descriptor.getClass().getSimpleName());
        assertEquals(JiraWorkflowSharingExporterImpl.class.getName(),descriptor.getModuleClass().getName());
        
    }

    @Test
    public void missingDescriptorReturnsEmptyCollection() throws Exception
    {
        Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(TestConstants.JUNK_CLASS);

        assertNotNull("descriptors was null but should have been an empty collection", descriptors);
        assertTrue("junk descriptor was found but should be empty", descriptors.isEmpty());
    }

    @Test
    public void systemWorkflowDescriptorReturnsEmptyCollection() throws Exception
    {
        
        Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(UpdateIssueFieldFunctionPluginFactory.class.getName());

        assertNotNull("descriptors was null but should have been an empty collection", descriptors);
        assertFalse("system descriptor was found but should be empty", !descriptors.isEmpty());

    }

    @Test
    public void customWorkflowDescriptorFound() throws Exception
    {
        File pluginFile = pluginTestUtils.getPluginFile(TestConstants.MY_POST_FUNCTION_PLUGIN_FILE);
        pluginTestUtils.uploadPluginViaUPM(pluginFile);
        try
        {
            Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(TestConstants.MY_POST_FUNCTION_CLASS);
    
            ModuleDescriptor descriptor = descriptors.iterator().next();
            
            //the descriptor is usually proxied by CGLib so we need to extract the non-proxied classname
            String descriptorClassname = StringUtils.substringBefore(descriptor.getClass().getName(),"$$EnhancerByCGLIB$$");
        
            assertEquals(TestConstants.MY_POST_FUNCTION_DESCRIPTOR_CLASS,descriptorClassname);
    
            AbstractWorkflowModuleDescriptor wfModuleDescriptor = (AbstractWorkflowModuleDescriptor) descriptor;
            
            assertEquals(TestConstants.MY_POST_FUNCTION_CLASS,wfModuleDescriptor.getImplementationClass().getName());
        }
        finally
        {
            Plugin plugin = pluginAccessor.getPlugin(TestConstants.MY_POST_FUNCTION_PLUGIN_KEY);
            pluginController.uninstall(plugin);
        }
        
    }

}
