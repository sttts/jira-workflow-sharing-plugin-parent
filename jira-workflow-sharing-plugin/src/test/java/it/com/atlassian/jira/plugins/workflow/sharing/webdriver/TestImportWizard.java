package it.com.atlassian.jira.plugins.workflow.sharing.webdriver;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowsPage;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.Options;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.*;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.ImportTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@WebTest({ Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.WORKFLOW })
@RestoreOnce("blankprojects.xml")
public class TestImportWizard extends JwspWebDriverTestCase
{
    @Test
    public void nonAdminCannotAccessImport() throws UnsupportedEncodingException
    {
        nonAdminCannotAccess(SelectWorkflow.URL);
    }

    @Test
    public void nonSysAdminCannotUploadFromComputer()
    {
        SelectWorkflow selectWorkflow = product.gotoLoginPage().login("jiraadmin", "jiraadmin", SelectWorkflow.class);

        assertTrue(selectWorkflow.isModesNotPresent());
        assertFalse(selectWorkflow.isNextButtonPresent());
        assertFalse(selectWorkflow.isBackButtonPresent());

        assertCancel(selectWorkflow);
    }

//TODO: since we are skipping the summary if there is nothing to be shown, we can no longer assume that the summary is shown
//    @Test
//    public void nothingToCreateInfoShownInSummary()
//    {
//        SelectWorkflow selectWorkflow = loginAsSysAdminAndGoToImport();
//        ChooseName chooseName = uploadBundleAndContinue(selectWorkflow, TestConstants.SIMPLE_JWB_FILE);
//        MapStatuses mapStatuses = chooseName.next();
//        Summary summary = mapStatuses.next();
//
//        assertTrue(summary.isNothingToCreateInfoShown());
//        assertFalse(summary.isStatusesListPresent());
//
//        mapStatuses = summary.back();
//        changeStatuses(mapStatuses, "-1", "-1");
//        summary = mapStatuses.next();
//
//        assertFalse(summary.isNothingToCreateInfoShown());
//        assertTrue(summary.isStatusesListPresent());
//
//        assertEquals(asList("Closed - 2", "Open - 2"), summary.getStatusNames());
//    }

    @Test
    public void testImport() throws IOException
    {
        ChooseName chooseName = assertWorkflowPageAsSysadmin();
        MapStatuses mapStatuses = assertChooseName(chooseName);
        Summary summary = assertMapStatuses(mapStatuses);
        WorkflowImported workflowImported = assertSummary(summary);
        assertWorkflowImported(workflowImported);
    }

    private SelectWorkflow loginAsSysAdminAndGoToImport()
    {
        return product.gotoLoginPage().loginAsSysAdmin(SelectWorkflow.class);
    }

    private ChooseName assertWorkflowPageAsSysadmin()
    {
        SelectWorkflow page = loginAsSysAdminAndGoToImport();

        assertCancel(page);
        assertFalse(page.isBackButtonPresent());
        assertTrue(page.isNextButtonPresent());

        assertTrue(page.isMarketplaceSelected());

        page.selectMarketplace();
        assertTrue(page.isMarketplaceContainerVisible());
        assertTrue(page.isNextButtonInvisible());
        assertTrue(page.isFileUploadInvisible());

        ChooseName chooseName = uploadBundleAndContinue(page);
        assertCurrentURL(chooseName.getUrl());

        return chooseName;
    }

    private MapStatuses assertChooseName(ChooseName page)
    {
        assertButtons(page);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        SelectWorkflow selectWorkflow = page.back();
        page = uploadBundleAndContinue(selectWorkflow);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        selectWorkflow = page.browserBack(1);
        page = selectWorkflow.browserForward(1);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        assertInvalidWorkflowName(page, " ", "You must specify a workflow name.");
        assertInvalidWorkflowName(page, Character.toString((char) 128), "Please use only ASCII characters for the workflow name.");
        assertInvalidWorkflowName(page, "With trailing whitespace ", "Workflow name cannot contain leading or trailing whitespaces.");

        page.setName("My Workflow");
        MapStatuses mapStatuses = page.next();
        assertCurrentURL(mapStatuses.getUrl());

        return mapStatuses;
    }

    private void assertInvalidWorkflowName(ChooseName page, String name, String expectedErrorMessage)
    {
        page.setName(name);
        page.next();
        assertCurrentURL(page.getUrl());
        assertTrue(page.isErrorMessageShown());
        assertEquals(expectedErrorMessage, page.getMessageParagraph());
    }

    private ChooseName uploadBundleAndContinue(SelectWorkflow page)
    {
        return uploadBundleAndContinue(page, TestConstants.SCREENS_FIELDS_NOTES_JWB_FILE);
    }

    private ChooseName uploadBundleAndContinue(SelectWorkflow page, String bundleName)
    {
        page.selectMyComputer();
        assertTrue(page.isUploadContainerVisible());
        assertTrue(page.isNextButtonVisible());
        assertFalse(page.isNextButtonEnabled());
        assertTrue(page.isFileUploadVisible());

        File bundle = ImportTestUtils.getJWBFile(bundleName);
        if (!bundle.exists())
        {
            throw new IllegalStateException("Bundle doesn't exist: " + bundle.getAbsolutePath());
        }
        page.setFile(bundle);
        assertTrue(page.isNextButtonEnabled());

        return page.next();
    }

    private Summary assertMapStatuses(MapStatuses page)
    {
        assertButtons(page);

        assertEquals(asList("Closed (6)", "Open (1)"), page.getOriginalStatusNames());

        ChooseName chooseName = page.back();
        assertEquals("My Workflow", chooseName.getName());
        chooseName.setName("My Workflow Changed");
        page = chooseName.next();

        assertAndChangeStatuses(page, "6", "1", "-1", "3");

        Summary summary = page.next();
        assertCurrentURL(summary.getUrl());

        return summary;
    }

    private WorkflowImported assertSummary(Summary page)
    {
        assertButtons(page);

        assertTrue(page.isPluginWarningShown());
        assertEquals(asList("JIRA Misc Workflow Extensions", "JIRA Suite Utilities", "JIRA Toolkit Plugin"), page.getPluginNames());

        assertTrue(page.isStatusesListPresent());
        assertEquals(asList("Closed - 2"), page.getStatusNames());
        assertEquals(asList("screenWithCustomField", "screenWithoutCustomField"), page.getScreensNames());
        assertFalse(page.isNothingToCreateInfoShown());

        assertCustomFields(page);

        MapStatuses mapStatuses = page.browserBack(1);
        assertAndChangeStatuses(mapStatuses, "-1", "3", "6", "1");
        page = mapStatuses.next();

        assertFalse(page.isStatusesListPresent());
        assertFalse(page.isNothingToCreateInfoShown());

        mapStatuses = page.back();
        assertAndChangeStatuses(mapStatuses, "6", "1", "6", "-1");
        page = mapStatuses.next();

        assertTrue(page.isStatusesListPresent());
        assertEquals(asList("Open - 2"), page.getStatusNames());

        WorkflowImported workflowImported = page.next();
        assertCurrentURL(workflowImported.getUrl());

        return workflowImported;
    }

    private void assertCustomFields(Summary page)
    {
        assertFalse(page.isCustomFieldWarningShown());
        assertTrue(page.isCustomFieldsTableShown());
        assertEquals(asList("myCascadingSelectField", "myTextField"), page.getCustomFieldNames());

        administration.plugins().disablePluginModule("com.atlassian.jira.plugin.system.customfieldtypes", "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect");
        product.getTester().getDriver().navigate().refresh();

        assertTrue(page.isCustomFieldWarningShown());
        assertEquals(asList("Cascading Select"), page.getCustomFieldWarningTypes());
        assertTrue(page.isCustomFieldsTableShown());
        assertEquals(asList("myTextField"), page.getCustomFieldNames());

        administration.plugins().disablePluginModule("com.atlassian.jira.plugin.system.customfieldtypes", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        product.getTester().getDriver().navigate().refresh();

        assertTrue(page.isCustomFieldWarningShown());
        assertEquals(asList("Cascading Select", "Text Field (< 255 characters)"), page.getCustomFieldWarningTypes());
        assertFalse(page.isCustomFieldsTableShown());
    }

    private void assertAndChangeStatuses(MapStatuses mapStatuses,
                                         String expectedCloseOptionValue, String expectedOpenOptionValue,
                                         String newCloseOptionValue, String newOpenOptionValue)
    {
        Option selectedClosedOption = mapStatuses.getClosedStatusSelect().getSelected();
        Option selectedOpenOption = mapStatuses.getOpenStatusSelect().getSelected();

        assertEquals(expectedCloseOptionValue, selectedClosedOption.value());
        assertEquals(expectedOpenOptionValue, selectedOpenOption.value());

        changeStatuses(mapStatuses, newCloseOptionValue, newOpenOptionValue);
    }

    private void changeStatuses(MapStatuses mapStatuses, String newCloseOptionValue, String newOpenOptionValue)
    {
        mapStatuses.getClosedStatusSelect().select(Options.value(newCloseOptionValue));
        mapStatuses.getOpenStatusSelect().select(Options.value(newOpenOptionValue));
    }

    private void assertWorkflowImported(WorkflowImported page) throws IOException
    {
        assertTrue(page.isSuccessMessageShown());
        assertEquals("Workflow has been successfully imported", page.getMessageTitle());

        assertTrue(page.isAdditionalConfigurationWarningShown());
        assertEquals(StringUtils.stripEnd(getExpectedTestNotes(), "\n"), page.getNotes());

        page.finish();
        product.getPageBinder().bind(WorkflowsPage.class);
        assertCurrentURL(getExpectedFinishUrl());

        product.getTester().getDriver().navigate().back();
        product.getTester().getDriver().navigate().back();

        SelectWorkflow selectWorkflow = product.getPageBinder().bind(SelectWorkflow.class);
        assertTrue(getCurrentUrl().contains("notFoundInSession=true"));
        assertTrue(selectWorkflow.isErrorMessageShown());
        assertEquals("Import wizard must be restarted.", selectWorkflow.getMessageParagraph());

        assertTrue(administration.workflows().goTo().inactive().contains("My Workflow Changed"));
    }

    @Override
    protected String getExpectedFinishUrl()
    {
        return new WorkflowsPage().getUrl();
    }
}
