package it.com.atlassian.jira.plugins.workflow.sharing.webdriver;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.plugin.util.ClassLoaderUtils;
import org.apache.commons.io.IOUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.WizardPage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static org.junit.Assert.*;

public abstract class JwspWebDriverTestCase extends NimbleFuncTestCase
{
    protected Backdoor backdoor;
    protected JiraTestedProduct product = new JiraTestedProduct(new EnvironmentBasedProductInstance());

    @Override
    public void beforeMethod()
    {
        super.beforeMethod();
        backdoor = new Backdoor(environmentData);

        product.getTester().getDriver().manage().window().maximize();
    }

    protected void nonAdminCannotAccess(String url) throws UnsupportedEncodingException
    {
        product.gotoLoginPage().login("jiradev", "jiradev", DashboardPage.class);

        String destination = product.getProductInstance().getBaseUrl() + url;

        product.getTester().getDriver().navigate().to(destination);

        String destinationUrl = URLEncoder.encode(destination, "UTF-8");
        assertCurrentURL("/login.jsp?permissionViolation=true&os_destination=" + destinationUrl);
    }

    protected void assertCurrentURL(String expectedFragment)
    {
        assertCurrentURL(expectedFragment, true);
    }

    private void assertCurrentURL(String expectedFragment, boolean assertEquals)
    {
        assertUrl(expectedFragment, getCurrentUrl(), assertEquals);
    }

    protected void assertUrl(String expectedFragment, String url)
    {
        assertUrl(expectedFragment, url, true);
    }

    private void assertUrl(String expectedFragment, String url, boolean assertEquals)
    {
        String expected = product.getProductInstance().getBaseUrl() + expectedFragment;
        if (assertEquals)
        {
            assertTrue(url.startsWith(expected));
        }
        else
        {
            assertFalse(url.startsWith(expected));
        }
    }

    protected String getCurrentUrl()
    {
        return product.getTester().getDriver().getCurrentUrl();
    }

    protected void assertButtons(WizardPage<?, ?> page)
    {
        assertCancel(page);
        assertTrue(page.isBackButtonPresent());
        assertTrue(page.isNextButtonPresent());
    }

    protected void assertCancel(WizardPage page)
    {
        assertUrl(getExpectedFinishUrl(), page.getCancelUrl());
    }

    protected abstract String getExpectedFinishUrl();


    protected String getExpectedTestNotes() throws IOException
    {
        return IOUtils.toString(ClassLoaderUtils.getResourceAsStream("test-notes.txt", getClass()));
    }

}
