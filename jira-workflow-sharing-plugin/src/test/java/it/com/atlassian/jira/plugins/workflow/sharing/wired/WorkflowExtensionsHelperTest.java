package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.google.common.collect.Multimap;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.PluginTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowExtensionsHelperTest
{

    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final WorkflowManager workflowManager;
    private final CustomFieldManager customFieldManager;
    private final WorkflowTestUtils workflowTestUtils;
    private final PluginTestUtils pluginTestUtils;
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;

    private CustomField builtInCF;

    public WorkflowExtensionsHelperTest(WorkflowExtensionsHelper workflowExtensionsHelper, WorkflowManager workflowManager, CustomFieldManager customFieldManager, WorkflowTestUtils workflowTestUtils, PluginTestUtils pluginTestUtils, PluginController pluginController, PluginAccessor pluginAccessor)
    {
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.workflowManager = workflowManager;
        this.customFieldManager = customFieldManager;
        this.workflowTestUtils = workflowTestUtils;
        this.pluginTestUtils = pluginTestUtils;
        this.pluginController = pluginController;
        this.pluginAccessor = pluginAccessor;
    }

    @BeforeClass
    public void setupData() throws GenericEntityException
    {
        this.builtInCF = createCustomField(TestConstants.TEXT_FIELD_CF_KEY, "myTextField");
    }

    @AfterClass
    public void removeData() throws RemoveException
    {
        customFieldManager.removeCustomField(builtInCF);
        workflowTestUtils.clean();
        pluginTestUtils.clean();
    }

    @Test
    public void requiredPluginsEmptyForNoExtensions() throws Exception
    {
        JiraWorkflow jw = workflowManager.getDefaultWorkflow();
        Multimap<Plugin, String> pluginsMap = workflowExtensionsHelper.getRequiredPlugins(jw);

        assertTrue("required plugins should be empty but was not", pluginsMap.isEmpty());
    }

    @Test
    public void requiredPluginsFoundForExtension() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", builtInCF);

        try
        {
            Multimap<Plugin, String> pluginsMap = workflowExtensionsHelper.getRequiredPlugins(jw);

            assertEquals(1, pluginsMap.size());
            
            Plugin thisPlugin = pluginsMap.keySet().iterator().next();
            assertEquals(TestConstants.JWSP_TEST_PLUGIN_KEY, thisPlugin.getKey());
            assertTrue(pluginsMap.containsEntry(thisPlugin, TestConstants.NOOP_POST_FUNCTION_CLASSNAME));
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void requiredPluginsFoundForCustomfield() throws Exception
    {
        JarPluginArtifact artifact = new JarPluginArtifact(pluginTestUtils.getPluginFile(TestConstants.SIMPLE_CF_PLUGIN_FILE));
        pluginController.installPlugins(artifact);

        CustomField pluginCF = createCustomField(TestConstants.SIMPLE_CF_PLUGIN_CF_KEY, "pluginField");

        JiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", pluginCF);

        try
        {
            Multimap<Plugin, String> pluginsMap = workflowExtensionsHelper.getRequiredPlugins(jw);

            Plugin thisPlugin = pluginAccessor.getPlugin(TestConstants.JWSP_TEST_PLUGIN_KEY);

            assertEquals(1, pluginsMap.size());
            assertTrue("postfunction plugin not found", pluginsMap.keySet().contains(thisPlugin));
            assertEquals(TestConstants.NOOP_POST_FUNCTION_CLASSNAME, pluginsMap.get(thisPlugin).iterator().next());
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
            customFieldManager.removeCustomField(pluginCF);
            
            Plugin plugin = pluginAccessor.getPlugin(TestConstants.SIMPLE_CF_PLUGIN_KEY);
            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    @Test
    public void extensionRemovedForExtensionClass() throws Exception
    {
        JarPluginArtifact artifact = new JarPluginArtifact(pluginTestUtils.getPluginFile(TestConstants.SIMPLE_CF_PLUGIN_FILE));
        pluginController.installPlugins(artifact);
        CustomField pluginCF = createCustomField(TestConstants.SIMPLE_CF_PLUGIN_CF_KEY, "pluginField");
        ConfigurableJiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", pluginCF);

        try
        {
            workflowExtensionsHelper.removeIllegalComponents(jw);

            Collection<ActionDescriptor> allActions = jw.getAllActions();

            for(ActionDescriptor action : allActions)
            {
                if(action.getName().equals("close issue"))
                {
                    List<FunctionDescriptor> functions = action.getPostFunctions();
                    functions.addAll(action.getUnconditionalResult().getPostFunctions());
                    for(FunctionDescriptor function : functions)
                    {
                        assertFalse("found custom post function but it should have been removed", TestConstants.NOOP_POST_FUNCTION_CLASSNAME.equals(function.getArgs().get("class.name")));
                    }
                }
            }

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
            customFieldManager.removeCustomField(pluginCF);

            Plugin plugin = pluginAccessor.getPlugin(TestConstants.SIMPLE_CF_PLUGIN_KEY);
            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    @Test
    public void extensionRemovedForCustomfieldClass() throws Exception
    {
        JarPluginArtifact artifact = new JarPluginArtifact(pluginTestUtils.getPluginFile(TestConstants.SIMPLE_CF_PLUGIN_FILE));
        pluginController.installPlugins(artifact);
        CustomField pluginCF = createCustomField(TestConstants.SIMPLE_CF_PLUGIN_CF_KEY, "pluginField");
        ConfigurableJiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", pluginCF);

        try
        {
            workflowExtensionsHelper.removeIllegalComponents(jw);

            Collection<ActionDescriptor> allActions = jw.getAllActions();

            for(ActionDescriptor action : allActions)
            {
                if(action.getName().equals("close issue"))
                {
                    List<FunctionDescriptor> functions = action.getPostFunctions();
                    functions.addAll(action.getUnconditionalResult().getPostFunctions());
                    for(FunctionDescriptor function : functions)
                    {
                        assertFalse("found custom post function but it should have been removed", TestConstants.NOOP_POST_FUNCTION_CLASSNAME.equals(function.getArgs().get("class.name")));
                    }
                }
            }

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
            customFieldManager.removeCustomField(pluginCF);

            Plugin plugin = pluginAccessor.getPlugin(TestConstants.SIMPLE_CF_PLUGIN_KEY);
            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    private CustomField createCustomField(String typeKey, String cfName) throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(typeKey);

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        return customFieldManager.createCustomField(cfName, "", cfType, null, Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
    }
}
