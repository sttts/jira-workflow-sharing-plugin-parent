package it.com.atlassian.jira.plugins.workflow.sharing.webdriver;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.AddNotes;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.ExportSuccess;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.StartExport;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

@WebTest({ Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.WORKFLOW })
@RestoreOnce("workflow-for-export.xml")
public class TestExportWizard extends JwspWebDriverTestCase
{
    private static final String WORKFLOW_NAME = "My Workflow";

    @Test
    public void nonAdminCannotAccessExport() throws UnsupportedEncodingException
    {
        nonAdminCannotAccess(new StartExport("My Workflow").getUrl());
    }

    @Test
    public void testExport() throws IOException, URISyntaxException
    {
        AddNotes addNotes = assertStartExport();
        ExportSuccess exportSuccess = assertAddNotes(addNotes);
        assertExportSuccess(exportSuccess);
    }

    private AddNotes assertStartExport()
    {
        StartExport page = product.gotoLoginPage().loginAsSysAdmin(StartExport.class, WORKFLOW_NAME);

        assertCancel(page);
        assertFalse(page.isBackButtonPresent());
        assertTrue(page.isNextButtonPresent());

        AddNotes addNotes = page.next();
        assertCurrentURL(addNotes.getUrl());

        return addNotes;
    }

    private ExportSuccess assertAddNotes(AddNotes page) throws IOException
    {
        assertButtons(page);

        assertEquals(getExpectedTestNotes(), page.getNotes());

        StartExport startExport = page.back("");
        page = startExport.next();

        assertEquals(getExpectedTestNotes(), page.getNotes());

        ExportSuccess exportSuccess = page.next();

        assertTrue(getCurrentUrl().startsWith(product.getProductInstance().getBaseUrl() + exportSuccess.getUrl()));

        return exportSuccess;
    }

    private void assertExportSuccess(ExportSuccess page) throws URISyntaxException, IOException
    {
        assertTrue(page.isSuccessMessageShown());
        assertTrue(page.isDownloadLinkShown());
        String downloadUrl = page.getDownloadUrl();
        assertFile(downloadUrl);

        page.finish();
        product.getPageBinder().bind(ViewWorkflowSteps.class, WORKFLOW_NAME);
        assertCurrentURL(getExpectedFinishUrl());

        product.getTester().getDriver().navigate().back();
        product.getTester().getDriver().navigate().back();

        StartExport startExport = product.getPageBinder().bind(StartExport.class, "");
        assertTrue(getCurrentUrl().contains("?notFoundInSession=true"));
        assertTrue(startExport.isErrorMessageShown());
    }

    private void assertFile(String downloadUrl) throws URISyntaxException, IOException
    {
        URI uri = new URI(downloadUrl);

        HttpGet loginMethod = new HttpGet(new URI(product.getProductInstance().getBaseUrl() + "/secure/Dashboard.jspa?os_username=admin&os_password=admin"));
        HttpGet downloadMethod = new HttpGet(uri);
        ClientConnectionManager httpConnectionManager = new BasicClientConnectionManager();
        DefaultHttpClient httpClient = new DefaultHttpClient(httpConnectionManager);

        try
        {
            HttpResponse loginResponse = httpClient.execute(loginMethod);
            EntityUtils.consumeQuietly(loginResponse.getEntity());

            HttpResponse response = httpClient.execute(downloadMethod);

            assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
            assertEquals("attachment; filename=\"My-Workflow.jwb\"", response.getHeaders("Content-Disposition")[0].getValue());
            assertEquals("application/zip;charset=UTF-8", response.getEntity().getContentType().getValue());

            EntityUtils.consumeQuietly(response.getEntity());
        }
        finally
        {
            loginMethod.releaseConnection();
            downloadMethod.releaseConnection();
            httpConnectionManager.shutdown();
        }
    }

    @Override
    protected String getExpectedFinishUrl()
    {
        return new ViewWorkflowSteps(WORKFLOW_NAME).getUrl();
    }
}
