package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugins.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.jira.plugins.workflow.sharing.exporter.WorkflowExportNotesProvider;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.PluginTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.*;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowSharingExporterTest
{
    private final JiraWorkflowSharingExporter jiraWorkflowSharingExporter;
    private final WorkflowTestUtils workflowTestUtils;
    private final WorkflowManager workflowManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserManager userManager;
    private final CustomFieldManager customFieldManager;
    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final FieldScreenManager fieldScreenManager;
    private final PluginTestUtils pluginTestUtils;
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;
    private final WorkflowExportNotesProvider exportNotesProvider;

    private CustomField builtInCF;
    private FieldScreen screenWithCustomField;

    public WorkflowSharingExporterTest(JiraWorkflowSharingExporter jiraWorkflowSharingExporter, WorkflowTestUtils workflowTestUtils,
                                       WorkflowManager workflowManager, JiraAuthenticationContext jiraAuthenticationContext, UserManager userManager,
                                       CustomFieldManager customFieldManager, ProjectManager projectManager, WorkflowSchemeManager workflowSchemeManager,
                                       FieldScreenManager fieldScreenManager, PluginTestUtils pluginTestUtils, PluginController pluginController,
                                       PluginAccessor pluginAccessor, WorkflowExportNotesProvider exportNotesProvider)
    {
        this.jiraWorkflowSharingExporter = jiraWorkflowSharingExporter;
        this.workflowTestUtils = workflowTestUtils;
        this.workflowManager = workflowManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userManager = userManager;
        this.customFieldManager = customFieldManager;
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.fieldScreenManager = fieldScreenManager;
        this.pluginTestUtils = pluginTestUtils; 
        this.pluginController = pluginController; 
        this.pluginAccessor = pluginAccessor;
        this.exportNotesProvider = exportNotesProvider;
    }

    @BeforeClass
    public void setupData() throws GenericEntityException
    {
        User user = userManager.getUserObject("admin");
        jiraAuthenticationContext.setLoggedInUser(user);

        this.builtInCF = createCustomField(TestConstants.TEXT_FIELD_CF_KEY, "myTextField");

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        screenWithCustomField = new FieldScreenImpl(fieldScreenManager);
        screenWithCustomField.setName("screenWithCustomField");
        screenWithCustomField.addTab("sometab");
        screenWithCustomField.getTab(0).addFieldScreenLayoutItem(builtInCF.getId());
    }

    @AfterClass
    public void removeData() throws RemoveException
    {
        fieldScreenManager.removeFieldScreen(screenWithCustomField.getId());
        customFieldManager.removeCustomField(builtInCF);
        workflowTestUtils.clean();
    }

    @Test
    public void simpleActiveWorkflowExports() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow("simpleWorkflow");

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("simpleWorkflow", "");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertEquals("found files other than workflow.xml, that's bad!", 1, explodedDir.list().length);

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void simpleActiveWorkflowWithNotesExports() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow("simpleWorkflow");

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("simpleWorkflow", "B flat");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");
            File notesFile = new File(explodedDir, "notes.md");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertTrue("notes file doesn't exist!", notesFile.exists());
            
            String notes = FileUtils.readFileToString(notesFile);
            assertEquals("wrong notes content", "B flat", notes);

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void simpleDraftWorkflowExports() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow("simpleWorkflow");
        Project testProject = projectManager.createProject("Test Project", "TST", "project for testing", "admin", "", AssigneeTypes.PROJECT_LEAD);
        GenericValue gv = workflowSchemeManager.createScheme("testWorkflowScheme","");
        
        Scheme workflowScheme = workflowSchemeManager.getSchemeObject((Long) gv.get("id"));
        try
        {
            GenericValue schemeGV = workflowSchemeManager.getScheme(workflowScheme.getId());
            
            for(IssueType issueType : testProject.getIssueTypes())
            {
                workflowSchemeManager.addWorkflowToScheme(schemeGV,"simpleWorkflow",issueType.getId());
            }

            workflowSchemeManager.addSchemeToProject(testProject,workflowScheme);
            workflowSchemeManager.clearWorkflowCache();
            
            
            workflowManager.createDraftWorkflow("admin", "simpleWorkflow");
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow("simpleWorkflow", "");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertEquals("found files other than workflow.xml, that's bad!", 1, explodedDir.list().length);

        }
        finally
        {
            workflowSchemeManager.removeSchemesFromProject(testProject);
            workflowSchemeManager.deleteScheme(workflowScheme.getId());
            workflowSchemeManager.clearWorkflowCache();
            projectManager.removeProject(testProject);
            workflowManager.deleteDraftWorkflow("simpleWorkflow");
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void missingActiveWorkflowReturnsNull() throws Exception
    {
        JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow("someMissingWorkflow", "");
        assertNull("jwb file was NOT null!", result);
    }

    @Test
    public void missingDraftWorkflowReturnsNull() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow("simpleWorkflow");

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow("simpleWorkflow", "");
            assertNull("jwb file was NOT null!", result);

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void workflowWithJustAPluginExports() throws Exception
    {
        JiraWorkflow jw = null;

        try
        {
            File pluginFile = pluginTestUtils.getPluginFile(TestConstants.MY_POST_FUNCTION_PLUGIN_FILE);
            pluginTestUtils.uploadPluginViaUPM(pluginFile);
            
            jw = workflowTestUtils.createMyPostFunctionWorkflow("myPostFunctionWorkflow");

            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("myPostFunctionWorkflow", notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, " - my-post-function", "Transition: close issue",
                    "Plugin post functions removed from this transition", " - My Post Function (my-post-function)");
        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }
            
            Plugin plugin = pluginAccessor.getPlugin(TestConstants.MY_POST_FUNCTION_PLUGIN_KEY);
            
            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
            
        }
    }
    
    @Test
    public void workflowWithFunctionAndFieldExports() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", builtInCF);

        try
        {
            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("noopFunctionWorkflow", notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, " - JIRA Workflow Sharing Plugin-tests", "Transition: close issue",
                    "Plugin post functions removed from this transition", " - NOOP CF Function (JIRA Workflow Sharing Plugin-tests)");
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void workflowWithIllegalSystemPostFunctions() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithIllegalSystemPostFunction("illegalSystemPostFunctions");

        try
        {
            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("illegalSystemPostFunctions", notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, "Transition: close issue",
                    "System post functions removed from this transition", " - Update Issue Field",
                    "Custom event fired by Fire Event Function was replaced by the generic event.");
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    private void assertNotes(File explodedDir, String... textsToVerify) throws IOException
    {
        File notesFile = new File(explodedDir, "notes.md");

        assertTrue("notes file doesn't exist", notesFile.exists());

        StringBuilder sb = new StringBuilder();
        BufferedReader notesReader = null;
        try
        {
            notesReader = new BufferedReader(new FileReader(notesFile));

            String line;
            while ((line = notesReader.readLine()) != null)
            {
                sb.append(line).append("\n");
            }
        }
        finally
        {
            if (notesReader != null)
            {
                notesReader.close();
            }
        }

        String notes = sb.toString();

        assertFalse("notes are blank", StringUtils.isBlank(notes));

        for (String textToVerify : textsToVerify)
        {
            assertTrue("expected text '" + textToVerify + "' was not found in notes \n" + notes, notes.contains(textToVerify));
        }
    }

    @Test
    public void workflowWithScreenAndFieldExports() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen("customScreenWorkflow", screenWithCustomField);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow("customScreenWorkflow", "");
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");
            File cfJson = new File(explodedDir, "customfields.json");
            File screenJson = new File(explodedDir, "screens.json");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertTrue("customfields json doesn't exist!", cfJson.exists());
            assertTrue("screens json doesn't exist!", screenJson.exists());

            //check custom fields
            ObjectMapper mapper = new ObjectMapper();
            List<CustomFieldInfo> cfInfoList = mapper.readValue(cfJson, new TypeReference<List<CustomFieldInfo>>() {});
            assertEquals("wrong number of customfields from json", 1, cfInfoList.size());
            assertEquals("wrong customfield", builtInCF.getId(), cfInfoList.get(0).getOriginalId());

            //check screens
            List<ScreenInfo> screenInfoList = mapper.readValue(screenJson, new TypeReference<List<ScreenInfo>>() {});
            assertEquals("wrong number of screens from json", 1, screenInfoList.size());
            assertEquals("wrong screen", screenWithCustomField.getId(), screenInfoList.get(0).getOriginalId());

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    private CustomField createCustomField(String typeKey, String cfName) throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(typeKey);

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        return customFieldManager.createCustomField(cfName, "", cfType, null, Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
    }
}
