package ut.com.atlassian.jira.plugins.workflow.sharing.utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

class ZipUtils
{
    static void unzip(final File zipFile, final File destDir) throws IOException
    {
        unzip(zipFile, destDir, 0);
    }
    
    /**
     * Unzips a file
     *
     * @param zipFile
     *            the Zip file
     * @param destDir
     *            the destination folder
     * @param leadingPathSegmentsToTrim
     *            number of root folders to skip. Example: If all files are in generated-resources/home/*,
     *            then you may want to skip 2 folders.
     * @throws java.io.IOException
     */
    private static void unzip(final File zipFile, final File destDir, int leadingPathSegmentsToTrim) throws IOException
    {
        final ZipFile zip = new ZipFile(zipFile);
        try
        {
            final Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements())
            {
                final ZipEntry zipEntry = entries.nextElement();
                String zipPath = trimPathSegments(zipEntry.getName(), leadingPathSegmentsToTrim);
                final File file = new File(destDir, zipPath);
                if (zipEntry.isDirectory())
                {
                    file.mkdirs();
                    continue;
                }
                // make sure our parent exists in case zipentries are out of order
                if (!file.getParentFile().exists())
                {
                    file.getParentFile().mkdirs();
                }

                InputStream is = null;
                OutputStream fos = null;
                try
                {
                    is = zip.getInputStream(zipEntry);
                    fos = new FileOutputStream(file);
                    IOUtils.copy(is, fos);
                }
                finally
                {
                    IOUtils.closeQuietly(is);
                    IOUtils.closeQuietly(fos);
                }
                file.setLastModified(zipEntry.getTime());
            }
        }
        finally
        {
            try
            {
                zip.close();
            }
            catch (IOException e)
            {
                // ignore
            }
        }
    }

    private static String trimPathSegments(String zipPath, final int trimLeadingPathSegments)
    {
        int startIndex = 0;
        for (int i = 0; i < trimLeadingPathSegments; i++)
        {
            int nextSlash = zipPath.indexOf("/", startIndex);
            if (nextSlash == -1)
            {
                break;
            }
            else
            {
                startIndex = nextSlash + 1;
            }
        }

        return zipPath.substring(startIndex);
    }
}
