package ut.com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.ScreenCreatorImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.apache.commons.lang.StringUtils.abbreviate;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScreenCreatorImplTest
{
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private FieldScreenManager fieldScreenManager;

    @Mock
    private FieldScreen fieldScreenOne;

    @Mock
    private FieldScreen fieldScreenTwo;

    private ScreenCreatorImpl screenCreator;

    @Before
    public void setUp()
    {
        ValidNameGenerator validNameGenerator = new ValidNameGenerator(jiraAuthenticationContext);

        screenCreator = new ScreenCreatorImpl(fieldScreenManager, validNameGenerator);

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void nameStaysTheSameIfThereIsNoExisting()
    {
        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne));
        when(fieldScreenOne.getName()).thenReturn("Something else");

        String name = screenCreator.getValidName("No duplicate");

        assertEquals("No duplicate", name);
    }

    @Test
    public void abbreviatesIfLengthIsExceeded()
    {
        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne));
        when(fieldScreenOne.getName()).thenReturn("Something else");

        String name = screenCreator.getValidName(StringUtils.repeat("a", 256));

        assertEquals(StringUtils.repeat("a", 252) + "...", name);
    }

    @Test
    public void addsPrefixOfTwoIfThereIsExisting()
    {
        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne));
        when(fieldScreenOne.getName()).thenReturn("Existing");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");

        String name = screenCreator.getValidName("Existing");

        assertEquals("Existing - 2", name);
    }

    @Test
    public void addsPrefixOfThreeOfIfThereIsExistingAndWithPrefixOfTwo()
    {
        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne, fieldScreenTwo));
        when(fieldScreenOne.getName()).thenReturn("Existing");
        when(fieldScreenTwo.getName()).thenReturn("Existing - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 3")).thenReturn("Existing - 3");

        String name = screenCreator.getValidName("Existing");

        assertEquals("Existing - 3", name);
    }

    @Test
    public void addsPrefixOfTwoAndAbbreviatesIfThereIsExistingAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 255);

        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne));
        when(fieldScreenOne.getName()).thenReturn(existingName);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");

        String abbreviatedName = abbreviate(existingName, 251);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");

        String name = screenCreator.getValidName(existingName);

        assertEquals(StringUtils.repeat("a", 248) + "... - 2", name);
    }

    @Test
    public void addsPrefixOfThreeAndAbbreviatesIfThereIsExistingAndWithPrefixOfTwoAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 255);
        String existingWithPrefixOfTwo= StringUtils.repeat("a", 248) + "... - 2";

        when(fieldScreenManager.getFieldScreens()).thenReturn(Arrays.asList(fieldScreenOne, fieldScreenTwo));
        when(fieldScreenOne.getName()).thenReturn(existingName);
        when(fieldScreenTwo.getName()).thenReturn(existingWithPrefixOfTwo);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");

        String abbreviatedName = abbreviate(existingName, 251);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 3")).thenReturn(abbreviatedName + " - 3");

        String name = screenCreator.getValidName(existingName);

        assertEquals(StringUtils.repeat("a", 248) + "... - 3", name);
    }
}
