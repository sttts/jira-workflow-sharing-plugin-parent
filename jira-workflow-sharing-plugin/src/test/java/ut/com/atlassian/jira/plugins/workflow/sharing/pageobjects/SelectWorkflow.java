package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import java.io.File;

public class SelectWorkflow extends WizardPage<SelectWorkflow, ChooseName>
{
    public static final String URL = "/plugins/servlet/wfshare-import";

    @ElementBy(id = "upm")
    private PageElement modeMarketplace;

    @ElementBy(id = "upload")
    private PageElement modeFromMyComputer;

    @ElementBy(id = "upm-container")
    private PageElement upmContainer;

    @ElementBy(id = "upload-container")
    private PageElement uploadContainer;

    @ElementBy(id = "wfShareImportFile")
    private PageElement fileUpload;

    @Override
    public String getUrl()
    {
        return URL;
    }

    public boolean isModesNotPresent()
    {
        return !modeMarketplace.isPresent() && !modeFromMyComputer.isPresent();
    }

    public boolean isMarketplaceSelected()
    {
        return modeMarketplace.isSelected();
    }

    public void selectMarketplace()
    {
        modeMarketplace.click();
    }

    public void selectMyComputer()
    {
        modeFromMyComputer.click();
    }

    public boolean isMarketplaceContainerVisible()
    {
        return isVisible(upmContainer, uploadContainer);
    }

    public boolean isUploadContainerVisible()
    {
        return isVisible(uploadContainer, upmContainer);
    }

    private boolean isVisible(PageElement visible, PageElement invisible)
    {
        return visible.isPresent() && visible.isVisible() && invisible.isPresent() && !invisible.isVisible();
    }

    public boolean isFileUploadVisible()
    {
        return fileUpload.isPresent() && fileUpload.isVisible();
    }

    public boolean isFileUploadInvisible()
    {
        return fileUpload.isPresent() && !fileUpload.isVisible();
    }

    public void setFile(File file)
    {
        fileUpload.type(file.getAbsolutePath());
    }

    @Override
    protected Class<ChooseName> getNextClass()
    {
        return ChooseName.class;
    }

    @Override
    protected Class<SelectWorkflow> getBackCass()
    {
        return SelectWorkflow.class;
    }
}
