package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import java.util.List;

public class Summary extends WizardPage<MapStatuses, WorkflowImported>
{
    @ElementBy(id = "summary-statuses")
    private PageElement statusesList;

    @ElementBy(id = "summary-screens")
    private PageElement screensTable;

    @ElementBy(id = "summary-custom-fields")
    private PageElement customFieldsTable;

    @ElementBy(id = "plugin-warning")
    private PageElement pluginWarning;

    @ElementBy(id = "custom-field-warning")
    private PageElement customFieldWarning;

    @ElementBy(id = "nothing-to-create-info")
    private PageElement nothingToCreateInfo;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/wfshare-import/summary";
    }

    @Override
    protected Class<WorkflowImported> getNextClass()
    {
        return WorkflowImported.class;
    }

    @Override
    protected Class<MapStatuses> getBackCass()
    {
        return MapStatuses.class;
    }

    public boolean isStatusesListPresent()
    {
        return statusesList.isPresent();
    }

    public List<String> getStatusNames()
    {
        return getNames(statusesList);
    }

    public List<String> getScreensNames()
    {
        return getNames(screensTable);
    }

    public boolean isCustomFieldsTableShown()
    {
        return customFieldsTable.isPresent();
    }

    public List<String> getCustomFieldNames()
    {
        return getNames(customFieldsTable);
    }

    private List<String> getNames(PageElement table)
    {
        List<PageElement> nameElements = table.findAll(By.className("summary-name"));
        return Lists.transform(nameElements, new PageElementToTestFunction());
    }

    public boolean isPluginWarningShown()
    {
        return pluginWarning.isVisible();
    }

    public List<String> getPluginNames()
    {
        List<PageElement> nameElements = pluginWarning.findAll(By.className("plugin-name"));
        return Lists.transform(nameElements, new PageElementToTestFunction());
    }

    public boolean isCustomFieldWarningShown()
    {
        return customFieldWarning.isPresent() && customFieldWarning.isVisible();
    }

    public List<String> getCustomFieldWarningTypes()
    {
        List<PageElement> nameElements = customFieldWarning.findAll(By.className("type-name"));
        return Lists.transform(nameElements, new PageElementToTestFunction());
    }

    private static class PageElementToTestFunction implements Function<PageElement, String>
    {
        @Override
        public String apply(PageElement element)
        {
            return element.getText();
        }
    }

    public boolean isNothingToCreateInfoShown()
    {
        return nothingToCreateInfo.isPresent() && nothingToCreateInfo.isVisible();
    }
}
