package ut.com.atlassian.jira.plugins.workflow.sharing.utils;

import com.atlassian.jira.plugins.workflow.sharing.importer.SharedWorkflowImportPlan;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.DefaultWorkflowBundleFactory;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowBundle;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class ImportTestUtils
{
    private final PluginAccessor pluginAccessor;
    private final WorkflowBundle.Factory bundleFactory;

    public ImportTestUtils(I18nResolver resolver, PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
        this.bundleFactory = new DefaultWorkflowBundleFactory(resolver);
    }

    public SharedWorkflowImportPlan getBasePlanForSimpleWorkflow() throws Exception
    {
        return extractBasePlanFromJWB(getJWBFileStream(TestConstants.SIMPLE_JWB_FILE));
    }

    public SharedWorkflowImportPlan getBasePlanForNoopFunctionWorkflow() throws Exception
    {
        return extractBasePlanFromJWB(getJWBFileStream(TestConstants.NOOP_FUNCTION_JWB_FILE));
    }

    public SharedWorkflowImportPlan getBasePlanForCustomScreenWorkflow() throws Exception
    {
        return extractBasePlanFromJWB(getJWBFileStream(TestConstants.CUSTOM_SCREEN_JWB_FILE));
    }

    public SharedWorkflowImportPlan getBasePlanForSimpleDraftWorkflow() throws Exception
    {
        return extractBasePlanFromJWB(getJWBFileStream(TestConstants.SIMPLE_JWB_DRAFT_FILE));
    }

    public SharedWorkflowImportPlan getBasePlanForMyPostFunctionWorkflow() throws Exception
    {
        return extractBasePlanFromJWB(getJWBFileStream(TestConstants.MY_POST_FUNCTION_JWB_FILE));
    }

    public SharedWorkflowImportPlan extractBasePlanFromJWB(InputStream stream) throws Exception
    {
        try
        {
            return new SharedWorkflowImportPlan(pluginAccessor, bundleFactory.bundle(stream, WorkflowBundle.BundleSource.MANUAL));
        }
        finally
        {
            IOUtils.closeQuietly(stream);
        }
    }

    public static InputStream getJWBFileStream(String filename)
    {
        String resourceName = FilenameUtils.concat(TestConstants.TEST_JWB_DIRECTORY, filename);
        return ClassLoaderUtils.getResourceAsStream(resourceName, ImportTestUtils.class);
    }

    public static File getJWBFile(String filename)
    {
        String resourceName = FilenameUtils.concat(TestConstants.TEST_JWB_DIRECTORY, filename);
        URL resource = ClassLoaderUtils.getResource(resourceName, ImportTestUtils.class);
        return new File(resource.getFile());
    }
}
