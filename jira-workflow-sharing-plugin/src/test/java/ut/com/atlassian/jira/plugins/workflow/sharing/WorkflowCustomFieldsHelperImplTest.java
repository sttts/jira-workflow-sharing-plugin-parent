package ut.com.atlassian.jira.plugins.workflow.sharing;

import java.util.Arrays;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.customfields.option.OptionsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugins.workflow.sharing.ModuleDescriptorLocator;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowCustomFieldsHelperImpl;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowPluginHelper;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WorkflowCustomFieldsHelperImplTest
{
    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private OfBizDelegator ofBizDelegator;

    @Mock
    private ModuleDescriptorLocator moduleDescriptorLocator;

    @Mock
    private WorkflowPluginHelper pluginHelper;

    @Mock
    private CustomField customField;

    private WorkflowCustomFieldsHelperImpl workflowCustomFieldsHelper;


    @Before
    public void setUp()
    {
        workflowCustomFieldsHelper = new WorkflowCustomFieldsHelperImpl(customFieldManager,
                moduleDescriptorLocator, pluginHelper);
    }

    @Test
    public void optionsAreEmptyIfNoConfigurationScheme()
    {
        when(customField.getConfigurationSchemes()).thenReturn(Lists.<FieldConfigScheme>newArrayList());

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertTrue(schemeInfo.getOptionInfo().isEmpty());
    }

    @Test
    public void optionsAreEmptyIfConfigurationSchemeHasNoOptions()
    {
        FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        when(scheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(scheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(null);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertTrue(schemeInfo.getOptionInfo().isEmpty());
    }

    @Test
    public void optionsAreRetrievedIfTheyExist()
    {
        FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        Option option = mock(Option.class);
        Options options = new OptionsImpl(Arrays.asList(option), null, null, null);

        when(scheme.isGlobal()).thenReturn(true);
        when(scheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(scheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(options);
        when(option.getValue()).thenReturn("My value");
        when(option.getOptionId()).thenReturn(1L);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertEquals(1, schemeInfo.getOptionInfo().size());
        assertEquals("My value", schemeInfo.getOptionInfo().get(0).getValue());
        assertEquals("1", schemeInfo.getOptionInfo().get(0).getOriginalId());
    }

    @Test
    public void globalOptionsAreRetrievedIfMultipleSchemes()
    {
        FieldConfigScheme nonGlobalScheme = mock(FieldConfigScheme.class);
        FieldConfigScheme globalScheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        Option option = mock(Option.class);
        Options options = new OptionsImpl(Arrays.asList(option), null, null, null);

        when(nonGlobalScheme.isGlobal()).thenReturn(false);
        when(globalScheme.isGlobal()).thenReturn(true);
        when(globalScheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(globalScheme, nonGlobalScheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(options);
        when(option.getValue()).thenReturn("My value");
        when(option.getOptionId()).thenReturn(1L);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertEquals(1, schemeInfo.getOptionInfo().size());
        assertEquals("My value", schemeInfo.getOptionInfo().get(0).getValue());
        assertEquals("1", schemeInfo.getOptionInfo().get(0).getOriginalId());
    }
}
