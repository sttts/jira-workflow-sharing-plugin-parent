package ut.com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.importer.component.DefaultWorkflowBundleFactory;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowBundle;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.google.common.collect.Iterables.isEmpty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class DefaultWorkflowBundleFactoryTest
{
    @Mock
    private I18nResolver resolver;

    private DefaultWorkflowBundleFactory factory;

    @Before
    public void setup()
    {
        when(resolver.getText(any(String.class))).thenAnswer(new Answer<String>()
        {
            @Override
            public String answer(InvocationOnMock invocationOnMock)
            {
                return (String) invocationOnMock.getArguments()[0];
            }
        });

        factory = new DefaultWorkflowBundleFactory(resolver);
    }

    @Test
    public void testStrings() throws IOException, ValidationException
    {
        String xml = "xml\u3044";
        String annotations = "annotations\u3045";
        String layouts = "layouts\u3046";
        String notes = "notes\u3047";

        BundleBuilder builder = new BundleBuilder().setWorkflowXml(xml)
                .setAnnotations(annotations)
                .setLayout(layouts)
                .setNotes(notes);

        WorkflowBundle bundle = factory.bundle(builder.build(), WorkflowBundle.BundleSource.MANUAL);
        assertEquals(WorkflowBundle.BundleSource.MANUAL, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
        assertEquals(annotations, bundle.getAnnotations());
        assertEquals(layouts, bundle.getLayout());
        assertEquals(notes, bundle.getNotes());
        assertTrue(isEmpty(bundle.getCustomFieldInfo()));
        assertTrue(isEmpty(bundle.getScreenInfo()));
        assertTrue(isEmpty(bundle.getPluginInfo()));

        builder = new BundleBuilder().setWorkflowXml("    " + xml + "\t\n\r")
                .setAnnotations("    ")
                .setLayout("    ")
                .setNotes("");

        bundle = factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);
        assertEquals(WorkflowBundle.BundleSource.MPAC, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
        assertNull(bundle.getAnnotations());
        assertNull(bundle.getLayout());
        assertNull(bundle.getNotes());
        assertTrue(isEmpty(bundle.getCustomFieldInfo()));
        assertTrue(isEmpty(bundle.getScreenInfo()));
        assertTrue(isEmpty(bundle.getPluginInfo()));
    }

    @Test
    public void testScreenInfo() throws IOException, ValidationException
    {
        String xml = "xml\u3044";

        ScreenInfo info1 = new ScreenInfo(10L, "name", "description", null);
        ScreenInfo infoDuplicate = new ScreenInfo(10L, "name2", "description", null);
        ScreenInfo info2 = new ScreenInfo(11L, "\u3044", "description", null);

        BundleBuilder builder = new BundleBuilder().setWorkflowXml(xml)
                .setScreenInfo(info1, info2, infoDuplicate);

        WorkflowBundle bundle = factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);

        assertEquals(WorkflowBundle.BundleSource.MPAC, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
        assertNull(bundle.getAnnotations());
        assertNull(bundle.getLayout());
        assertNull(bundle.getNotes());
        assertTrue(isEmpty(bundle.getCustomFieldInfo()));
        assertTrue(isEmpty(bundle.getPluginInfo()));

        assertEquals(2, Iterables.size(bundle.getScreenInfo()));
        assertComparatorEquals(ImmutableList.of(info1, info2), bundle.getScreenInfo(), new Comparator<ScreenInfo>()
        {

            @Override
            public int compare(ScreenInfo o1, ScreenInfo o2)
            {
                return new CompareToBuilder()
                        .append(o1.getName(), o2.getName())
                        .append(o1.getDescription(), o2.getDescription())
                        .append(o1.getOriginalId(), o2.getOriginalId())
                        .toComparison();
            }
        });
    }

    @Test
    public void testFieldInfo() throws IOException, ValidationException
    {
        String xml = "xml\u3044";

        CustomFieldInfo info1 = new CustomFieldInfo("10", "name", "description", "module", "searcher", null, "plugin");
        CustomFieldInfo info1duplicate = new CustomFieldInfo("10", "name2", "description2", "module2", "searcher2", null, "plugin2");
        CustomFieldInfo info2 = new CustomFieldInfo("11", "\u3044name3", "description3", "module3", "searcher3", null, "plugin3");

        BundleBuilder builder = new BundleBuilder().setWorkflowXml(xml)
                .setCustomFieldInfo(info1, info2, info1duplicate);

        WorkflowBundle bundle = factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);

        assertEquals(WorkflowBundle.BundleSource.MPAC, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
        assertNull(bundle.getAnnotations());
        assertNull(bundle.getLayout());
        assertNull(bundle.getNotes());
        assertTrue(isEmpty(bundle.getScreenInfo()));
        assertTrue(isEmpty(bundle.getPluginInfo()));

        assertEquals(2, Iterables.size(bundle.getCustomFieldInfo()));
        assertComparatorEquals(ImmutableList.of(info1, info2), bundle.getCustomFieldInfo(), new Comparator<CustomFieldInfo>()
        {

            @Override
            public int compare(CustomFieldInfo o1, CustomFieldInfo o2)
            {
                return new CompareToBuilder()
                        .append(o1.getName(), o2.getName())
                        .append(o1.getDescription(), o2.getDescription())
                        .append(o1.getOriginalId(), o2.getOriginalId())
                        .append(o1.getSearcherModuleKey(), o2.getSearcherModuleKey())
                        .append(o1.getPluginKey(), o2.getPluginKey())
                        .append(o1.getTypeModuleKey(), o2.getTypeModuleKey())
                        .toComparison();
            }
        });
    }

    @Test
    public void testPluginInfo() throws IOException, ValidationException
    {
        String xml = "xml\u3044";

        WorkflowExtensionsPluginInfo info1 = new WorkflowExtensionsPluginInfo("name", "key", "description", "version", "atlassian", null, "panel");
        WorkflowExtensionsPluginInfo info1duplicate = new WorkflowExtensionsPluginInfo("name2", "key", "description2", "version2", "atlassian2", "sds", "panel2");
        WorkflowExtensionsPluginInfo info2 = new WorkflowExtensionsPluginInfo("\u3044", "key2", "description2", "version2", "atlassian2", null, "panel2");

        BundleBuilder builder = new BundleBuilder().setWorkflowXml(xml)
                .setPluginInfo(info1, info2, info1duplicate);

        WorkflowBundle bundle = factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);

        assertEquals(WorkflowBundle.BundleSource.MPAC, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
        assertNull(bundle.getAnnotations());
        assertNull(bundle.getLayout());
        assertNull(bundle.getNotes());
        assertTrue(isEmpty(bundle.getScreenInfo()));
        assertTrue(isEmpty(bundle.getCustomFieldInfo()));

        assertEquals(2, Iterables.size(bundle.getPluginInfo()));
        assertComparatorEquals(ImmutableList.of(info1, info2), bundle.getPluginInfo(), new Comparator<WorkflowExtensionsPluginInfo>()
        {

            @Override
            public int compare(WorkflowExtensionsPluginInfo o1, WorkflowExtensionsPluginInfo o2)
            {
                return new CompareToBuilder()
                        .append(o1.getName(), o2.getName())
                        .append(o1.getDescription(), o2.getDescription())
                        .append(o1.getKey(), o2.getKey())
                        .append(o1.getArtifactName(), o2.getArtifactName())
                        .append(o1.getVersion(), o2.getVersion())
                        .append(o1.getVendorUrl(), o2.getVendorUrl())
                        .toComparison();
            }
        });
    }

    @Test
    public void noWorkflow() throws IOException
    {
        BundleBuilder builder = new BundleBuilder().setWorkflowXml("");

        try
        {
            factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);
            fail("Expected exception");
        }
        catch (ValidationException e)
        {
            assertEquals("wfshare.exception.workflow.xml.not.found.in.zip", e.getMessage());
        }
    }

    @Test
    public void badZip() throws IOException
    {
        byte[] data = StringUtils.repeat("5747", 1000).getBytes("UTF-8");
        InputStream notZip = new ByteArrayInputStream(data);

        try
        {
            factory.bundle(notZip, WorkflowBundle.BundleSource.MPAC);
            fail("Expected exception");
        }
        catch (ValidationException e)
        {
            assertEquals("wfshare.exception.file.must.be.a.zip.file", e.getMessage());
        }
    }

    @Test
    public void entryTooBig() throws IOException
    {
        String xml = "brenden";
        byte[] data = xml.getBytes("UTF-8");

        BundleBuilder builder = new BundleBuilder()
                .store().setWorkflowXml(xml);

        factory = new DefaultWorkflowBundleFactory(resolver, Long.MAX_VALUE, data.length - 1);
        try
        {
            factory.bundle(builder.build(), WorkflowBundle.BundleSource.MPAC);
            fail("Expected exception");
        }
        catch (ValidationException e)
        {
            assertEquals("wfshare.exception.workflow.too.big", e.getMessage());
        }
    }

    @Test
    public void fileTooBig() throws IOException
    {
        String xml = "brenden";

        BundleBuilder builder = new BundleBuilder()
                .store().setWorkflowXml(xml);

        final ByteArrayInputStream build = builder.build();
        factory = new DefaultWorkflowBundleFactory(resolver, xml.length(), Long.MAX_VALUE);
        try
        {
            factory.bundle(build, WorkflowBundle.BundleSource.MPAC);
            fail("Expected exception");
        }
        catch (ValidationException e)
        {
            assertEquals("wfshare.exception.workflow.too.big", e.getMessage());
        }
    }

    @Test
    public void tooJustSmallEnough() throws IOException, ValidationException
    {
        String xml = "brenden";

        BundleBuilder builder = new BundleBuilder()
                .store().setWorkflowXml(xml);

        final ByteArrayInputStream build = builder.build();
        factory = new DefaultWorkflowBundleFactory(resolver, build.available(), xml.length());
        final WorkflowBundle bundle = factory.bundle(build, WorkflowBundle.BundleSource.MPAC);

        assertEquals(WorkflowBundle.BundleSource.MPAC, bundle.getSource());
        assertEquals(xml, bundle.getWorkflowXml());
    }

    private <T> void assertComparatorEquals(Iterable<T> source, Iterable<T> target, Comparator<? super T> comparator)
    {
        Iterator<T> targetIter = target.iterator();
        int count = 0;
        for (T left : source)
        {
            if (!targetIter.hasNext())
            {
                Assert.fail("Target is shorter then the source.");
            }
            T right = targetIter.next();
            if (comparator.compare(left, right) != 0)
            {
                Assert.fail(String.format("%s != %s at position %d.", left, right, count));
            }
            count++;
        }
        if (targetIter.hasNext())
        {
            Assert.fail("Source is shorter then the source.");
        }
    }

    private static class BundleBuilder
    {
        private final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        private final ZipOutputStream outputStream = new ZipOutputStream(stream);
        private final ObjectMapper mapper;

        private boolean stored = false;

        public BundleBuilder()
        {
            mapper = new ObjectMapper();
            mapper.getJsonFactory().configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        }

        public BundleBuilder setWorkflowXml(String xml)
        {
            writeFile("workflow.xml", xml);
            return this;
        }

        public BundleBuilder setLayout(String layout)
        {
            writeFile("layout.json", layout);
            return this;
        }

        public BundleBuilder setAnnotations(String annotations)
        {
            writeFile("annotation.json", annotations);
            return this;
        }

        public BundleBuilder setNotes(String notes)
        {
            writeFile("notes.md", notes);
            return this;
        }

        public ByteArrayInputStream build()
        {
            try
            {
                outputStream.close();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            return new ByteArrayInputStream(stream.toByteArray());
        }

        public void writeObject(String name, Object object)
        {
            try
            {
                final ByteArrayOutputStream data = new ByteArrayOutputStream();
                mapper.writeValue(data, object);
                writeData(name, data.toByteArray());
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        public void writeFile(String name, String xml)
        {
            try
            {
                writeData(name, xml.getBytes("UTF-8"));
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        public void writeData(String name, byte[] data)
        {
            ZipEntry entry = new ZipEntry(name);
            if (stored)
            {
                entry.setMethod(ZipOutputStream.STORED);
                entry.setSize(data.length);
                entry.setCompressedSize(data.length);
                CRC32 crc32 = new CRC32();

                crc32.update(data);
                entry.setCrc(crc32.getValue());
            }
            try
            {
                outputStream.putNextEntry(entry);
                outputStream.write(data);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        public BundleBuilder setCustomFieldInfo(CustomFieldInfo... info)
        {
            writeObject("customfields.json", Arrays.asList(info));
            return this;
        }

        public BundleBuilder setScreenInfo(ScreenInfo... info)
        {
            writeObject("screens.json", Arrays.asList(info));
            return this;
        }

        public BundleBuilder setPluginInfo(WorkflowExtensionsPluginInfo... info)
        {
            writeObject("plugins.json", Arrays.asList(info));
            return this;
        }

        public BundleBuilder store()
        {
            stored = true;
            return this;
        }
    }
}
