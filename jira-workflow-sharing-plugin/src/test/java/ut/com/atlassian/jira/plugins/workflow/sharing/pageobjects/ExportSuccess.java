package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class ExportSuccess extends WizardPage<AddNotes, ExportSuccess>
{
    @ElementBy(id = "finishButton")
    private PageElement finishButton;

    @ElementBy(id = "download-bundle-link")
    private PageElement downloadLink;

    @Override
    public TimedCondition isAt()
    {
        return finishButton.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/wfshare-export/export-workflow-success";
    }

    @Override
    protected Class<ExportSuccess> getNextClass()
    {
        return ExportSuccess.class;
    }

    @Override
    protected Class<AddNotes> getBackCass()
    {
        return AddNotes.class;
    }

    public void finish()
    {
        finishButton.click();
    }

    public boolean isDownloadLinkShown()
    {
        return downloadLink.isVisible();
    }

    public String getDownloadUrl()
    {
        return downloadLink.getAttribute("href");
    }
}
