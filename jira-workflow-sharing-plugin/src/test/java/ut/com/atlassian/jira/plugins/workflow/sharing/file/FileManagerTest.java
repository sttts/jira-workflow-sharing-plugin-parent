package ut.com.atlassian.jira.plugins.workflow.sharing.file;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManager;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManagerImpl;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileManagerTest
{
    @Mock
    public JiraHome home;

    private FileManager fileManager;
    private File tempDir;

    @Before
    public void setup()
    {
        tempDir = Files.createTempDir();
        assertTrue(tempDir.exists());
        assertTrue(tempDir.isDirectory());

        when(home.getExportDirectory()).thenReturn(new File(tempDir, "export"));
        when(home.getImportDirectory()).thenReturn(new File(tempDir, "import"));

        fileManager = new FileManagerImpl(home);
    }

    @After
    public void cleanup()
    {
        try {
            FileUtils.deleteDirectory(tempDir);
        } catch (IOException e) {
        }
    }


    @Test
    public void testExportWorkflowFiles() throws IOException, CanNotCreateFileException
    {
        File exportedWorkflowFile = fileManager.createExportedWorkflowFile();
        assertTrue(exportedWorkflowFile.createNewFile());

        String fileName = exportedWorkflowFile.getName();
        assertTrue(StringUtils.isAlphanumeric(fileName));

        File file = fileManager.getExportedWorkflowFile(fileName);
        assertEquals(exportedWorkflowFile, file);

        fileManager.delete(file);
        assertFalse(exportedWorkflowFile.exists());
    }

    @Test
    public void testExportWorkflowFileNames()
    {
        fileManager.addExportedFileNameMapping("actual", "display");
        assertEquals("display", fileManager.getExportFileNameToDisplay("actual"));

        assertNull(fileManager.getExportFileNameToDisplay("does not exist"));

        fileManager.clearExportFileNameMapping("actual");
        assertNull(fileManager.getExportFileNameToDisplay("actual"));
    }

    @Test
    public void testAllFileDeletion() throws IOException, CanNotCreateFileException
    {
        File exportedWorkflowFile = fileManager.createExportedWorkflowFile();
        assertTrue(exportedWorkflowFile.createNewFile());
        fileManager.deleteAllWorkflowFiles();
    }

    @Test
    public void testOldFileDeletion() throws IOException, InterruptedException, CanNotCreateFileException
    {
        File exportedWorkflowFileOld = fileManager.createExportedWorkflowFile();
        File exportedWorkflowFileNew = fileManager.createExportedWorkflowFile();

        final long now = System.currentTimeMillis();
        assertTrue(exportedWorkflowFileOld.createNewFile());
        assertTrue(exportedWorkflowFileOld.setLastModified(now - TimeUnit.MINUTES.toMillis(10)));

        assertTrue(exportedWorkflowFileNew.createNewFile());
        assertTrue(exportedWorkflowFileNew.setLastModified(now));

        assertTrue(exportedWorkflowFileNew.exists());
        assertTrue(exportedWorkflowFileOld.exists());

        fileManager.clearOlderThan(TimeUnit.MINUTES.toMillis(5));

        assertTrue(exportedWorkflowFileNew.exists());
        assertFalse(exportedWorkflowFileOld.exists());
    }
}
