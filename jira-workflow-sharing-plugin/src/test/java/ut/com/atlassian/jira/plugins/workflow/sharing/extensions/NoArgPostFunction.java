package ut.com.atlassian.jira.plugins.workflow.sharing.extensions;

import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import java.util.Map;

public class NoArgPostFunction extends AbstractJiraFunctionProvider
{

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        //really, NOOP means we do nothing!!
    }
}
