package ut.com.atlassian.jira.plugins.workflow.sharing.importer.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.workflow.sharing.file.FileCleaningJobScheduler;
import com.atlassian.jira.plugins.workflow.sharing.file.FileManager;
import com.atlassian.jira.plugins.workflow.sharing.importer.SharedWorkflowImportPlan;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.*;
import com.atlassian.jira.plugins.workflow.sharing.importer.servlet.ImportWizardHandler;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ServletMapping;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.plugins.workflow.sharing.importer.servlet.ImportWizardHandler.SessionVar;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ImportWizardHandlerTest
{
    @Mock
    private LoginUriProvider loginUriProvider;
    @Mock
    private WebSudoManager webSudoManager;
    @Mock
    private UserManager userManager;
    @Mock
    private TemplateRenderer renderer;
    @Mock
    private WorkflowImporterFactory importerFactory;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private I18nResolver i18n;
    @Mock
    private WorkflowStatusHelper workflowStatusHelper;
    @Mock
    private VelocityRequestContextFactory requestContextFactory;
    @Mock
    private PacBundleDownloader pacFileDownloader;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private ComponentAccessor.Worker worker;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private ScreenCreator screenCreator;
    @Mock
    private FileManager fileManager;
    @Mock
    private FileCleaningJobScheduler fileCleaningJobScheduler;
    @Mock
    private XsrfTokenValidator xsrfTokenValidator;
    @Mock
    private XsrfTokenGenerator xsrfTokenGenerator;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private WorkflowBundle.Factory bundleFactory;

    @Mock
    private WorkflowBundle bundle;

    private SharedWorkflowImportPlan plan;

    private ImportWizardHandler importWizardHandler;


    @Before
    public void setUp()
    {
        importWizardHandler = new ImportWizardHandler(loginUriProvider, webSudoManager, userManager, renderer, importerFactory,
                workflowManager, i18n, workflowStatusHelper, requestContextFactory, pacFileDownloader,
                screenCreator, bundleFactory, xsrfTokenValidator, xsrfTokenGenerator, applicationProperties, projectManager, pluginAccessor);

        when(bundle.getCustomFieldInfo()).thenReturn(Collections.<CustomFieldInfo>emptyList());
        plan = new SharedWorkflowImportPlan(pluginAccessor, bundle);

        when(request.getSession()).thenReturn(session);

        ComponentAccessor.initialiseWorker(worker);
        when(worker.getComponent(JiraAuthenticationContext.class)).thenReturn(jiraAuthenticationContext);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getBaseUrl()).thenReturn("");
    }

    @Test
    public void blankWorkflowNameIsNotAllowed() throws Exception
    {
        when(session.getAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name())).thenReturn(plan);

        testWorkflowNameAndAssertError("");
        testWorkflowNameAndAssertError(" ");
    }

    @Test
    public void leadingAndTrailingSpacesInWorkflowNameAreNotAllowed() throws Exception
    {
        when(session.getAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name())).thenReturn(plan);

        testWorkflowNameAndAssertError("");
        testWorkflowNameAndAssertError(" WF with leading whitespace");
        testWorkflowNameAndAssertError("WF with trailing whitespace ");
    }

    @Test
    public void existingWorkflowNameIsNotAllowed() throws Exception
    {
        when(session.getAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name())).thenReturn(plan);
        when(workflowManager.getWorkflow("TEST")).thenReturn(new ConfigurableJiraWorkflow(null, null));

        testWorkflowNameAndAssertError("TEST");
    }

    private void testWorkflowNameAndAssertError(String name) throws Exception
    {
        when(request.getParameter(ImportWizardHandler.WF_NAME_FIELD_NAME)).thenReturn(name);

        Map<String, Object> context = new HashMap<String, Object>();

        importWizardHandler.doSetName(request, response, context);

        assertTrue(context.containsKey("errorMessage"));
    }

    @Test
    public void validWorkflowNameRedirectsToStatuses() throws Exception
    {
        when(session.getAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name())).thenReturn(plan);

        when(request.getParameter(ImportWizardHandler.WF_NAME_FIELD_NAME)).thenReturn("TEST");

        Map<String, Object> context = new HashMap<String, Object>();

        importWizardHandler.doSetName(request, response, context);

        assertEquals("TEST", plan.getWorkflowName());

        verify(response).sendRedirect(ServletMapping.IMPORT_MAP_STATUSES.getPath());
    }
}
