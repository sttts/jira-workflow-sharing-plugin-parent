package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

public abstract class WizardPage<P extends WizardPage, N extends WizardPage> extends AbstractJiraPage
{
    @ElementBy(className = "cancel")
    private PageElement cancelLink;

    @ElementBy(id = "nextButton")
    private PageElement nextButton;

    @ElementBy(id = "backButton")
    private PageElement backButton;

    @ElementBy(cssSelector = ".aui-message")
    private PageElement message;

    @Override
    public TimedCondition isAt()
    {
        return cancelLink.timed().isPresent();
    }

    public boolean isNextButtonPresent()
    {
        return nextButton.isPresent();
    }

    public boolean isNextButtonVisible()
    {
        return isNextButtonPresent() && nextButton.isVisible();
    }

    public boolean isNextButtonInvisible()
    {
        return isNextButtonPresent() && !nextButton.isVisible();
    }

    public boolean isNextButtonEnabled()
    {
        return nextButton.isEnabled();
    }

    public boolean isBackButtonPresent()
    {
        return backButton.isPresent();
    }

    public N next()
    {
        nextButton.click();

        return pageBinder.bind(getNextClass());
    }

    public P back(Object... args)
    {
        backButton.click();

        return pageBinder.bind(getBackCass(), args);
    }

    public N browserForward(int times)
    {
        for (int i = 0; i < times; i++)
        {
            driver.navigate().forward();
        }

        return pageBinder.bind(getNextClass());
    }

    public P browserBack(int times)
    {
        for (int i = 0; i < times; i++)
        {
            driver.navigate().back();
        }

        return pageBinder.bind(getBackCass());
    }

    protected abstract Class<N> getNextClass();

    protected abstract Class<P> getBackCass();

    public String getCancelUrl()
    {
        return cancelLink.getAttribute("href");
    }

    public void cancel()
    {
        cancelLink.click();
    }

    public boolean isErrorMessageShown()
    {
        return message.isVisible() && message.hasClass("error");
    }

    public boolean isSuccessMessageShown()
    {
        return message.isVisible() && message.hasClass("success");
    }

    public String getMessageTitle()
    {
        return message.find(By.cssSelector("div.title strong")).getText();
    }

    public String getMessageParagraph()
    {
        return message.find(By.tagName("p")).getText();
    }
}
