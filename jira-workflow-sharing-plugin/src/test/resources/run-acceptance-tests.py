#!/usr/bin/python
import argparse
import os.path
import re
import os
import subprocess
import stat

def execute_tests(options):
    subprocess.call("mkdir -p src/test", shell=True)
    subprocess.call("cp -R target/test-classes src/test/resources", shell=True)

    process_args = [get_maven_executable(), "-B", "integration-test", "-Djira.version=%s" % options.jira_version]
    process_args += options.others

    process_args_str = ' '.join(process_args);
    process_args_str = process_args_str.replace(" -- ", " ")

    print "Executing: %s" % process_args_str

    return subprocess.call(process_args_str, shell=True)


def get_maven_executable():
    mvn = "mvn"
    if os.environ.has_key('M2_HOME'):
        mvn = "%s/bin/mvn" % os.environ['M2_HOME']
        if not os.path.isfile(mvn):
            raise Exception("Maven executable not found in %s" % mvn)

    return mvn

def call_assert_success(args):
    print "Executing: %s" % args
    flush_output()
    res = subprocess.call(args)
    if res is not 0:
        raise Exception("Subprocess returned non zero result: %s" % res)


def flush_output():
    sys.stdout.flush()
    sys.stderr.flush()

def main():
    parser = argparse.ArgumentParser(description="Runs acceptance tests")

    # jira versions settings
    parser.add_argument("-j", "--jira-version", action="store", dest="jira_version", required=True,
        help="Sets JIRA version to test against")
    parser.add_argument("-d", "--jira-data-version", action="store", dest="jira_data_version",
        help="Sets JIRA data version to test against. Optional: as default jira_version will be used.")
    parser.add_argument("-t", "--jira-test-version", action="store", dest="jira_test_version",
        help="Sets test library version to test against. Optional: as default jira_version will be used.")

    # additional options
    parser.add_argument("-i", "--install-plugin", action="store_true", dest="install_plugin",
        help="Tells AMPS to install plugin")

    parser.add_argument("others", nargs='*', default=[], help="Other arguments - those arguments will be passed to maven as the last")

    options = parser.parse_args()

    if options.jira_data_version is None:
        options.jira_data_version = options.jira_version

    if options.jira_test_version is None:
        options.jira_test_version = options.jira_version

    execute_tests(options)

if __name__ == '__main__':
    main()
