#!/bin/sh

if [ -f "${M2_HOME}/bin/mvn" ]; then
  MVN=${M2_HOME}/bin/mvn
else
  MVN=mvn
fi

$MVN -q exec:java -Dexec.classpathScope="compile" -Dexec.mainClass=com.atlassian.jira.plugins.workflow.sharing.importer.component.BundleChecker -Dexec.args="$*"
